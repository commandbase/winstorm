<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $title; ?> | WINstorm Presents Inc</title>
    <link rel="stylesheet" href="assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
  </head>
  <body>
    <header>
      <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
          <a class="navbar-brand" href="/">
            <img src="assets/images/winstorm-logo.png" alt="WINstorm" />
          </a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" href="/index.php">Example 1</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/example2.php">Example 2</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/example3.php">Example 3</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/example4.php">Example 4</a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>