<?php $title = 'Example 2'; include 'header.php' ?>

<section class="example-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 center">
        Starting Number: <span class="numbers">7,2,6,4,3,3,10,,1,4,5,2</span><br>
        <a href="#" id="sortBTN" class="common-btn">Sort numbers lowest to highest, and remove duplicates & empty entries</a>
      </div>
    </div>
  </div>
</section>

<?php include 'footer.php' ?>
