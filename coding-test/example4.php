<?php $title = 'Example 4'; include 'header.php'; ?>

<section class="example-4">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 center">
        <p>Erase value to check different json statuses:</p>
        <ul>
          <li><label for="user">User</label> <input type="text" id="user" name="user" value="1234" /></li>

          <li><label for="client">Client</label> <input type="text" id="client" name="client" value="TestCo" /></li>

          <li><label for="action">Action</label> <input type="text" id="action" name="action" value="disableUser" /></li>
        </ul>

        <a href="#" id="checkBTN" class="common-btn">Check Results</a>

        <p id="form_message"></p>
      </div>
    </div>
  </div>
</section>

<?php include 'footer.php' ?>