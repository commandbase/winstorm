<?php
// Example 3
if(isset($_POST) && !empty($_POST)) {

  // Create DB Connection
  include 'db_conn.php';

  // Prep jQuery vars
  $client = $mysqli->real_escape_string($_POST['client']);
  $user = $mysqli->real_escape_string($_POST['user']);
  $action = ($_POST['action'] == 'login') ? 'logged in' : 'logged out';

  // Update database record that matches the jQuery vars
  $mysqli->query("UPDATE UserStatus SET status = '$action' WHERE client = '$client' AND user = '$user'");
  
  // Close connection
  $mysqli->close();

  // Return results
  echo $action;
}
?>