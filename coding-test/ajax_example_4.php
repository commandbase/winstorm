<?php
// Example 4
if(isset($_POST) && !empty($_POST)) {

  // Process variables / response
  if(!isset($_POST['user']) || $_POST['user'] == '' || !isset($_POST['client']) || $_POST['client'] == '') {
    $data = [ 'result' => 'fail', 'reason' => 'Not Authorized' ];
  } elseif(!isset($_POST['action']) || $_POST['action'] == '') {
    $data = [ 'result' => 'fail', 'reason' => 'No Action Specified' ];
  } else {
    $data = [ 'result' => 'success' ];
  }

  // Return results
  header('Content-type: application/json');
  echo json_encode( $data );
}
?>