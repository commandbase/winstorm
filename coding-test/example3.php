<?php $title = 'Example 3'; include 'header.php' ?>

<?php
// Create DB Connection
include 'db_conn.php';

// Return name of current default database
if ($result = $mysqli->query("SELECT status FROM UserStatus WHERE client = 'WINstorm' AND user = 'JOEY'")) {
  $status = $result->fetch_row();
  $result->close();
}

$mysqli->close();
?>

<section class="example-3">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 center">
        <a href="#" id="loginBTN" class="common-btn" <?php echo ($status[0] == 'logged in') ? ' style="display:none;"' : ''; ?>>Login</a>
        <a href="#" id="logoutBTN" class="common-btn" <?php echo ($status[0] == 'logged out') ? ' style="display:none;"' : ''; ?>>Logout</a>
      </div>
    </div>
  </div>
</section>

<?php include 'footer.php' ?>