$(document).ready(function() {
  'use strict';

  // Eample 1
  $('#exampleBTN').on('click', function (e) {
    e.preventDefault();

    // Change images to gray scale for 2 seconds
    $('.example-1 .container .row div:even').addClass('gray').delay(2000).queue(function(){
      $(this).removeClass('gray').dequeue();
    });

  });

  // Eample 2
  $('#sortBTN').on('click', function (e) {
    e.preventDefault();

    // Sort numbers lowest to highest, and remove duplicates & empty entries
    $('.example-2 .numbers').text(unique($('.example-2 .numbers').text().split(',').filter(function(v){return v!==''})).sort(function(a, b) {
      return a - b;
    }));

    /******************************
     * Code explained
     ******************************/

    // Get string of numbers
    // var numbers = $('.example-2 .numbers').text();
    
    // Split comma delimited list to array 
    // var num_array = numbers.split(',');

    // Remove empties
    // var new_array = num_array.filter(function(v){return v!==''});

    // Remove duplicates
    // var num_unique = unique(new_array);

    // Sort array
    // num_unique.sort(function(a, b) {
    //   return a - b;
    // });
    
    // Return value
    // $('.example-2 .numbers').text(num_unique);

  });

  // Eample 3
  $('#loginBTN').on('click', function (e) {
    e.preventDefault();

    var client = 'WINstorm', 
        user = 'Joey',
        action = 'login';

    $.ajax({
      url: 'ajax.php',
      data: {client:client,user:user,action:action},
      type: 'POST',
      success: function (data) {
        if(data == 'logged out') {
          $('#loginBTN').show();
          $('#logoutBTN').hide();
        } else {
          $('#loginBTN').hide();
          $('#logoutBTN').show();
        }
      }
    });

  });

  $('#logoutBTN').on('click', function (e) {
    e.preventDefault();

    var client = 'WINstorm', 
        user = 'Joey',
        action = 'logout';

    $.ajax({
      url: 'ajax.php',
      data: {client:client,user:user,action:action},
      type: 'POST',
      success: function (data) {
        if(data == 'logged out') {
          $('#loginBTN').show();
          $('#logoutBTN').hide();
        } else {
          $('#loginBTN').hide();
          $('#logoutBTN').show();
        }
      }
    });

  });

  // Eample 4
  $('#checkBTN').on('click', function (e) {
    e.preventDefault();

    var client = $('#client').val(), 
        user = $('#user').val(),
        action = $('#action').val();

    $.ajax({
      url: 'ajax_example_4.php',
      data: {client:client,user:user,action:action},
      type: 'POST',
      success: function (data) {
        if(data.result == 'success') {
          $('#form_message').text('You successfully submitted all values.');
        } else if(data.result == 'fail') {
          if(data.reason == 'Not Authorized') {
            $('#form_message').text('We could not authorize you, please make sure but the User and Client values are filled out.');
          } else if(data.reason == 'No Action Specified') {
            $('#form_message').text('No action was filled out, please fill out and action and try again.');
          } else {
            $('#form_message').text('Generic error message.');
          }
        }
      }
    });

  });


});

function unique(list) {
  var result = [];
  $.each(list, function(i, e) {
    if ($.inArray(e, result) == -1) result.push(e);
  });
  return result;
}
