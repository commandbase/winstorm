<?php $title = 'Example 1'; include 'header.php' ?>

<section class="example-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>
      <div class="col-lg-3 col-md-4 col-sm-6">
        <img src="assets/images/cat.jpg" alt="Cat" />
      </div>

      <div class="col-lg-12 center">
        <a href="#" id="exampleBTN" class="common-btn">Grayscale odd images</a>
      </div>
    </div>
  </div>
</section>

<?php include 'footer.php' ?>