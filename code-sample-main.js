// Global var
var CMDB = {};

// Lazy load all images flagged with the "lazy" class
var lazyLoadInstance = new LazyLoad({
  elements_selector: '.lazy',
  threshold: 100, // Load within px of scroll to element
  class_loading: "lazy-loading",
  class_loaded: "lazy-loaded",
  class_error: "lazy-error",
});

/* ========================================================================
* Global Helpers
* ======================================================================== */

/**
 * Gets an item value from local storage
 *
 * @param {string} key
 * @return {mixed}
 */
function getStorageItem(key) {
  if (typeof(Storage) !== "undefined") {

    var value = localStorage.getItem(key);

    if ( ! value ) {
      return false;
    }

    value = JSON.parse(value);

    // Check to see if the item as expired, then delete if so
    if ( typeof(value.expiry) !== "undefined" && value.expiry && value.expiry < (new Date().getTime()) ) {
      localStorage.removeItem(key);
      return false;
    }

    try {
      var return_val = JSON.parse(value.value);
    }
    catch(err) {
      var return_val = value.value;
    }

    return return_val;

  }
  else {
    console.log('This browser does not support local storage');
    return false;
  }
}

/**
 * Gets an item value from local storage
 *
 * @param {string} key
 * @param {mixed} value
 * @param {int} expiry - days/hours from now it will expire
 * @param {int} keep_expiry - keeps expiry from previous value if possible
 * @param {int} expiry_measurement - either days or hours
 * @return {mixed}
 */
function setStorageItem(key, value, expiry, keep_expiry, expiry_measurement) {
  if (typeof(Storage) !== "undefined") {

    // Figure timestamp to expire at
    if( typeof(expiry) !== "undefined" ) {

      var today = new Date();

      // Default measurement = days
      if( typeof(expiry_measurement) == "undefined" || expiry_measurement == 'days' ) {
        today.setDate(today.getDate() + expiry);
        expiry = today.getTime();
      }
      // Hours
      else if( expiry_measurement == 'hours' ) {
        today.setTime(today.getTime() + (expiry * 60 * 60 * 1000));
        expiry = today.getTime();
      }
      else {
        console.log('Unknown expiry_measurement: ' + expiry_measurement);
      }
      
    }
    else {
      expiry = 0; // 0 is never
    }
    
    expiry = typeof(expiry) !== "undefined" ? expiry : 0;

    // Keeps the expiry of the previous value
    var prev_expiry = false;
    if ( typeof(expiry) !== "undefined" && keep_expiry ) {
      var prev_value = localStorage.getItem(key);

      if ( prev_value ) {

        prev_value = JSON.parse(prev_value);

        // Check to see if the item as expired, then delete if so
        prev_expiry = typeof(prev_value.expiry) !== "undefined" && prev_value.expiry ? prev_value.expiry : false;

      }
    }

    var value_object = {
      'timestamp': new Date().getTime(),
      'expiry': prev_expiry ? prev_expiry : expiry,
      'value': value
    }

    return localStorage.setItem(key, JSON.stringify(value_object));

  }
  else {
    console.log('This browser does not support local storage');
    return false;
  }
}

/**
 * Deletes a local storage item
 *
 * @param key
 */
function deleteStorageItem(key) {
  localStorage.removeItem(key);
}

// Looks at the favorites cookie, and updates the fav count in the header
function updateFavoriteCount() {
  var favorites = getStorageItem( 'favorites' );
  if( favorites ) {
    $('.favorites-count span').text( favorites.length );
  }
  else {
    $('.favorites-count span').text( '0' );
  }
}
updateFavoriteCount();

(function ($) {

  // USE STRICT
  "use strict";

  /* ========================================================================
  * PREDEFINED VARIABLES
  * ======================================================================== */

  var $window = $(window),
    $document = $(document),
    $body = $('body');

  /* ========================================================================
  * UTILITIES
  * ======================================================================== */

  // Check if function exists
  $.fn.exists = function () {
      return this.length > 0;
  };

    // urlParam helper
  function getParameterByName( name, string ){
    var regexS = "[\\?&]"+name+"=([^&#]*)", 
    regex = new RegExp( regexS ),
    results = regex.exec( string );
    if( results == null ){
      return "";
    } else{
      return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  }

  function isTablet(width) {
    if(width == undefined){
      width = 991;
    }
    if(window.innerWidth <= width) {
      return true;
    } else {
      return false;
    }
  }

  function isMobile(width) {
    if(width == undefined){
      width = 767;
    }
    if(window.innerWidth <= width) {
      return true;
    } else {
      return false;
    }
  }

  function formatDate(date, format) {
    var monthNames = [
      "Jan", "Feb", "Mar",
      "Apr", "May", "Jun", 
      "Jul", "Aug", "Sep",
      "Oct", "Nov", "Dec"
    ];

    var weekday = [
      "Sun", "Mon", "Tue", 
      "Wed", "Thu", "Fri",
      "Sat"
    ];

    var wd = weekday[date.getDay()];
    var day = date.getDate();
    var month = monthNames[date.getMonth()];
    var year = date.getFullYear();

    if(format == 'year') {
      return wd + ', ' + month + ' ' + day + ' ' + year;  
    }
    else {
      return wd + ', ' + month + ' ' + day;
    }
    
  }

/* ========================================================================
   * World Map Popup
   * Show Popup when you click on World Map Marker
   * ======================================================================== */
  CMDB.worldMapPopup = function () {

    // init first item as active.
    $('.map-world .map-location-0').css({
        'display' : 'block'
    });
    $(".map-world .map>a").first().find('img').addClass('active');

    var markers = $('.map-world .datamaps-marker');

    markers.on('click', function (e) {
      e.preventDefault();
      // remove active classes on market images.
      markers.find('img').removeClass('active');
      // active active class to the newly selected marker
      $(this).find('img').addClass('active');

      // Get marker map ID
      var mapId = ($(this).attr('aria-mapId'));
      // Hide all popups before opening a new one
      $('.map-world .map-location').hide();
      // Show the popup for this marker
      if ( typeof mapId !== 'undefined' && mapId != '' ) {
        $('.map-world .map-location-'+mapId).show();
      }
    });

    $('.map-world .map-location .close').on('click', function () {
      // When clicking the close button close all popups
      $('.map-world .map-location').hide();
    });
  };




/* ========================================================================
   * Tour "Add to Favorites" icon/button
   * ======================================================================== */

   // Add to favorites links/buttons
    $(document.body).on('click', '.tour-box-favorite', function(event) {
        event.preventDefault();
        var current_favorites = getStorageItem( 'favorites' );
        var post_id = parseInt($(this).data('post-id'));

        if( !current_favorites ) {
            current_favorites = [];
        }

        if (current_favorites.indexOf(post_id) !== -1) {
            // Remove it.
            $(this).find('.fi').removeClass('active');
            var index = current_favorites.indexOf( post_id );
            if (index > -1) {
                current_favorites.splice(index, 1);
            }
            setStorageItem( 'favorites', JSON.stringify( current_favorites ) );
        } else {
            // Add it.
            $(this).find('.fi').addClass('active');
            current_favorites.push( post_id );
            setStorageItem( 'favorites', JSON.stringify( current_favorites ) );
        }

        if ( $(this).data('title') ) {
            dataLayer.push({
                'event': 'add_to_favorites',
                'tour': $(this).data('title')
            });
        }

        updateFavoriteCount();
    });


/* ========================================================================
   * Tour "Add to compare tool" icon/button
   * ======================================================================== */

    // Add to compare tool links/buttons
    $(document.body).on('click', '.compare-tour, .tour .remove, .remove-item .remove', function(event) {
        var current_compare = getStorageItem( 'compare' );
        var post_id = parseInt($(this).data('post-id'));

        if( !current_compare ) {
            current_compare = [];
        }

        if (current_compare.indexOf(post_id) !== -1) {
            // Remove it.
            $('#compare-tour-' + post_id).prop('checked', false);
            var index = current_compare.indexOf( post_id );
            if (index > -1) {
                current_compare.splice(index, 1);
            }
            setStorageItem('compare', JSON.stringify(current_compare));
            CMDB.loadCompareTool();

            if ($(this).parent().hasClass('remove-item') && current_compare.length > 0) {
                openCompare();
            }
        } else {
            // Add it.
            if (current_compare.length < 4) {
                $(this).find('.custom-control-input').prop('checked', true);
                current_compare.push(post_id);
                setStorageItem('compare', JSON.stringify(current_compare));
                CMDB.loadCompareTool();
            } else {
                $(this).find('.custom-control-input').prop('checked', false);
            }
        }

        if (current_compare.length > 1) {
            $('#compare-tour .compare-tours a').removeClass('disabled').text('Compare Tours');
        } else {
            $('#compare-tour .compare-tours a').addClass('disabled').text('Add a Tour to Compare');
        }

        if ( $(this).data('title') ) {
            dataLayer.push({
                'event': 'add_to_compare',
                'tour': $(this).data('title')
            });
        }
    });

    // Add click event for compare tool modal.
    $(document.body).on('click', '#compare-tour .compare-tours a', function(event) {
        event.preventDefault();
        if (!$(this).hasClass('disabled')) {
            openCompare();
        }
    });

    function openCompare() {
        $('body').addClass('modal-open');

        // Get all tours to compare.
        var compare = getStorageItem( 'compare' );
        var tours_total = compare.length;

        $.ajax({
            url: '/wp/wp-admin/admin-ajax.php',
            data: {action:'compare_tool_modal',tours:compare},
            type: 'POST',
            success: function (data) {
                $('#compare-tour-modal .compare-tour-modal--body').html(data);
                $('#compare-tour-modal .tour-count .current').html(tours_total);
                $('#compare-tour-modal').fadeIn();
            }
        });
    }

    $(document.body).on('click', '.compare-tour-modal-close', function(event) {
        event.preventDefault();
        closeCompare();
    });

    function closeCompare() {
        $('body').removeClass('modal-open');
        $('#compare-tour-modal').fadeOut();
    }


/* ========================================================================
   * Country Map Popup
   * Show Popup when you click on World Map Marker
   * ======================================================================== */

  CMDB.countryMapPopup = function () {
    $('.map-country .datamaps-marker').on('click', function (e) {
      e.preventDefault();
    });

    $('.map-country .map-location .close').on('click', function () {
      // When clicking the close button close all popups
      $('.map-country .datamaps-marker').tooltipster('close');
      $('.map-country .map-marker').attr('src', '/content/themes/base/img/map-off.png');
    });


  	// http://iamceege.github.io/tooltipster/
    $('.map-country .datamaps-marker').tooltipster({
    	repositionOnScroll: true,
    	trigger: 'click',
    	interactive: true,
    	// side: ['left', 'right', 'bottom', 'top'],
    	trackOrigin: true,
    	theme: 'tooltipster-shadow',
    	zIndex: 99,
			functionInit: function(instance, helper){
				var originID = $(helper.origin).attr('aria-mapId');
				var sides = $('#country-map-popup-'+originID).attr('data-arrow');
				// console.log(sides);
				instance.option('side', sides);
			},
			functionBefore: function(instance, helper) {
				var originID = $(helper.origin).attr('aria-mapId');
				$('#country-map-popup-'+originID).show();
				// $(helper.origin).find('img').attr('src', '/content/themes/base/img/map-on.png');
				$(helper.origin).find('img').addClass('active');
			},
			functionAfter: function(instance, helper) {
				var originID = $(helper.origin).attr('aria-mapId');
				// $(helper.origin).find('img').attr('src', '/content/themes/base/img/map-off.png');
				$(helper.origin).find('img').removeClass('active');
			},
    });
  };


/* ========================================================================
   * Region Map Popup
   * Show Popup when you click on World Map Marker
   * ======================================================================== */

  CMDB.regionMapPopup = function () {
    $('.map-region .datamaps-marker').on('click', function (e) {
      e.preventDefault();
    });

    $('.map-region .map-location .close').on('click', function () {
      // When clicking the close button close all popups
      $('.map-region .datamaps-marker').tooltipster('close');
    });


  	// http://iamceege.github.io/tooltipster/
    $('.map-region .datamaps-marker').tooltipster({
    	repositionOnScroll: true,
    	trigger: 'click',
    	interactive: true,
    	// side: ['left', 'right', 'bottom', 'top'],
    	trackOrigin: true,
    	theme: 'tooltipster-shadow',
    	zIndex: 99,
			functionInit: function(instance, helper){
				var originID = $(helper.origin).attr('aria-mapId');
				var sides = $('#region-map-popup-'+originID).attr('data-arrow');
				// console.log(sides);
				instance.option('side', sides);
			},
			functionBefore: function(instance, helper) {
				var originID = $(helper.origin).attr('aria-mapId');
				$('#region-map-popup-'+originID).show();
				// $(helper.origin).find('img').attr('src', '/content/themes/base/img/map-on.png');
				$(helper.origin).find('img').addClass('active');
			},
			functionAfter: function(instance, helper) {
				var originID = $(helper.origin).attr('aria-mapId');
				// $(helper.origin).find('img').attr('src', '/content/themes/base/img/map-off.png');
				$(helper.origin).find('img').removeClass('active');
			},
    });
  };

/* ========================================================================
   * Popup search box
   * Show search box on click
   * ======================================================================== */

  CMDB.searchBtn = function () {
    $('.search-btn').on('click', function () {
      // $('.navbar-toggler').addClass('collapsed');
      $("#navbarDropdown").attr("aria-expanded","true");
      $("#navbarDropdown").attr("aria-haspopup","false");
      $("#mobilemenu").removeClass('show');
      $('.mobile-menu').hide();
      $('#search').toggle();
    });
  };
  

/* ========================================================================
   * Mega Menu Stuff
   * ======================================================================== */

  CMDB.megaMenuStuff = function () {
    //Hamburger mobilemenu
    jQuery('#mobile-menu-toggle').on('click', function(e){
      e.preventDefault();
      jQuery('#mobile-menu-toggle').toggleClass('open');
      // jQuery('.main-menu').stop().slideToggle();
      jQuery('.main-menu').slideToggle();
    });
    //Submenu toggle
    jQuery('.toggleSubMenu').on('click', function(e){
      e.preventDefault();
      
      if (!isTablet()) {

				if (jQuery('.toggleSubMenu').not(this).hasClass('active')) {
	        jQuery('.toggleSubMenu.active').find('i.for-desktop').removeClass('caret-up');
	        // jQuery('.toggleSubMenu.active').find('i.for-desktop').removeClass('flaticon-caret-down');
	        jQuery('.toggleSubMenu.active').parents('.menu-item').find('.sub-menu-wrapper').removeClass('open');
	      	jQuery('.toggleSubMenu.active').removeClass('active');
				}

	      if (jQuery(this).hasClass('active')) {
	      	jQuery(this).removeClass('active');
	        jQuery(this).find('i.for-desktop').removeClass('caret-up');
	        // jQuery(this).find('i.for-desktop').removeClass('flaticon-caret-down');
	        jQuery(this).parents('.menu-item').find('.sub-menu-wrapper').removeClass('open');
	      }else{
	      	jQuery(this).addClass('active');
	        jQuery(this).find('i.for-desktop').addClass('caret-up');
	        // jQuery(this).find('i.for-desktop').addClass('flaticon-caret-down');
	        jQuery(this).parents('.menu-item').find('.sub-menu-wrapper').addClass('open');
	      }

      }else{
      	// If mobile menu...
	      if (jQuery(this).hasClass('active')) {
	      	jQuery(this).removeClass('active');
	        jQuery(this).find('i.for-mobile').removeClass('caret-up');
	        // jQuery(this).find('i.for-mobile').addClass('flaticon-caret-down');
	        jQuery(this).parents('.menu-item').find('.sub-menu-wrapper').slideToggle();
	      }else{
	      	jQuery(this).addClass('active');
	        jQuery(this).find('i.for-mobile').addClass('caret-up');
	        // jQuery(this).find('i.for-mobile').removeClass('flaticon-caret-down');
	        jQuery(this).parents('.menu-item').find('.sub-menu-wrapper').slideToggle();
	      }
      }
    });

		// Dismiss on click outside menu
		$(document).click(function(event) {
			// if ($('.sub-menu-wrapper').hasClass('open')) {
			  var target = $(event.target);
				if (!$(target).hasClass('toggleSubMenu')) {
				  if(!target.closest('.sub-menu-wrapper').length && $('.sub-menu-wrapper').is(":visible")) {
			      if (!isTablet()) {
			        jQuery('.toggleSubMenu.active').find('i.for-desktop').removeClass('caret-up');
			        // jQuery('.toggleSubMenu.active').find('i.for-desktop').removeClass('flaticon-caret-down');
			        jQuery('.toggleSubMenu.active').parents('.menu-item').find('.sub-menu-wrapper').removeClass('open');
			      	jQuery('.toggleSubMenu.active').removeClass('active');
						}
					}
			  }
			// }
		});

    //Header Search Bar Toggle
    // jQuery('.header-search-button').on('click', function(e){
    //   e.preventDefault();
    //   jQuery(this).toggleClass('open');
    //   jQuery('.top-search-bar').slideToggle();
    // });
    // //Custom search button submit
    // jQuery('.mobile-search-submit').on('click', function(e){
    //   e.preventDefault();
    //   jQuery(this).parents('form').submit();
    // });

		// Automatic menu builder for post type hirerarchy setup
		// Get the first menu list and show it's menu items
    $('.custom-mm-auto-list-left .custom-mm-left-trigger').first().addClass('active');
    var activeID = $('.custom-mm-auto-list-left .custom-mm-left-trigger.active').attr('data-destination-id');
    $('.custom-mm-auto-list-right .region-box[data-region-id="'+activeID+'"]').first().show();
		
		// sub menu menu system data attributes setup
    $('.custom-mm-auto-list-left .custom-mm-left-trigger').each(function(index, el) {
    	var theID = $(this).attr('data-destination-id');
    	var theURL = $(this).attr('data-url');
    	var theTitle = $(this).attr('data-title');
    	$('.custom-mm-auto-list-right .region-box[data-region-id="'+theID+'"] .region-box-header').append('<a class="region-box-header-link" href="'+theURL+'">Explore '+theTitle+'</a>');
    });

    // sub menu menu system toggle
    $('.custom-mm-left-trigger').on('click', function(e){
    	e.preventDefault();
    	var theID = $(this).data('destination-id');
    	if (!isTablet()) {
	    	$('.custom-mm-auto-list-right .region-box').removeClass('active').hide();
	    	$('.custom-mm-left-trigger').removeClass('active');
	    	$(this).addClass('active');
	    	$(".custom-mm-auto-list-right .region-box[data-region-id='"+theID+"']").addClass('active').show();
    	}else{
    		// var theURL = $(this).attr('data-url');
    		// window.location.href = theURL;

    		var theHref = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(theHref).offset().top
        }, 300);

    	}

    })
  };

/* ========================================================================
 * Mobile Menu Toggle
 * Open/Close Mobile Menu on Click
 * ======================================================================== */
  // CMDB.navbarToggler = function () {
    // $('.navbar-toggler').on('click', function () {
    //   $('.search-button').addClass('collapsed');
    //   $(".search-button").attr("aria-expanded","true");
    //   $(".search-btn").attr("aria-haspopup","false");
    //   $("#mobilemenu").addClass('show');
    //   $('.mobile-menu').show();
    //   $('#search').hide();
    // });
  // };
  

/* ========================================================================
   * Sticky Header Menu
   * On scroll make the header menu stick to the top
   * ======================================================================== */
  CMDB.stickyMenu = function () {
  	if (!isTablet()) {
	    $(window).scroll(function() {
	      var topheader = $('#header .top-bar').outerHeight();
	      // var headerheight = $('#header').height();
	      // var mainhight = headerheight - topheader;
	      var myheight = $(window).scrollTop();
	      if(myheight > topheader){
	        $('#header .menu-bar').addClass('headersticky');
	      }
	      if(myheight < topheader){
	        $('#header .menu-bar').removeClass('headersticky');
	      }
	      if(myheight == 0){
	        $('#header .menu-bar').removeClass('headersticky');
	      }    		
	    });
  	}
  };


  /* ========================================================================
   * Sticky Itinerary Nav
   * On scroll make the Itinerary menu stick to the top
   * ======================================================================== */
    CMDB.itineraryMenu = function (reinit) {
       //console.log('itin menu init');
      if ($('.itinerary-nav').length) {

					if (reinit == true) {
						// $('.itinerary-nav-inner').unstick();
						$('.itinerary-nav-inner').sticky('update');
					}else{
					  $('.itinerary-nav-inner').on('sticky-start', function() {
					  	 //console.log("Started");
					  	$('.itinerary-nav-inner').addClass('fixed');
					  	// CMDB.scrollSpyCheck();
					  });
					  $('.itinerary-nav-inner').on('sticky-end', function() {
					  	 //console.log("Ended");
					  	// $('.itinerary-mobile-nav').removeClass('active');
					  	$('.itinerary-nav-inner').removeClass('fixed');
					  	// CMDB.scrollSpyCheck();
					  });
					}


          // if (isTablet()) {
          if (window.innerWidth < 992) {
          	//console.log('tablet');

            $('.itinerary-nav-inner').sticky({topSpacing:0});

          }else{
          	//console.log('desktop');

            //var headerheight = $('.headersticky').outerHeight();
            // height never changes from 100 so hardcoded, above ^ returns undefined
            var headerheight = 100;

            $('.itinerary-nav-inner').sticky({
            	topSpacing: headerheight
            });

          }

      }
    };

  /* ========================================================================
   * SVG to Inline SVG
   * Replace all SVG images with inline SVG
   * ======================================================================== */
  CMDB.svgToInlineSVG = function () {

    $('img.svg').each(function(){
      var $img = $(this);
      var imgID = $img.attr('id');
      var imgClass = $img.attr('class');
      var imgURL = $img.attr('src');

      $.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = $(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if(!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'));
        }

        // Replace image with new SVG
        $img.replaceWith($svg);

      }, 'xml');

    });

  };


  /* ========================================================================
   * Owl Carousel
   * ======================================================================== */

  CMDB.owlCarouselInit = function () {
    
    // Initiate Carousel - our travelers
    var travelersCarousel = $('.travelers-carousel');
    travelersCarousel.owlCarousel({
    	items: 1,
      loop: true,
      nav: true,
      margin: 5,
      autoplay: false,
      dots: false,
      // autoWidth: true,
      // autoplayTimeout: 3500,
      // autoplayHoverPause: true,
      // navSpeed: 1300,
      center: true,
      // autoplaySpeed: 1300,
      navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
      responsiveClass:true,
      lazyLoad: true,
      onInitialized: function(event) {
        // REVALIDATE
        lazyLoadInstance.update();
      },
      onTranslate: function(event) {
				// REVALIDATE
				lazyLoadInstance.update();
			},
      responsive: {
        0: {
          items: 1
        },
        640: {
          items: 2
        },
        992: {
          items: 3
        },
        1199: {
          items: 4
        }
      }
    });

    // Content List Tours carousels init
    var contentListToursCarousel = $('.content-list-tours-carousel');
    contentListToursCarousel.owlCarousel({
      items:1,
      slideBy: 1,
      loop:false,
      dots: false,
      nav: true,
      lazyLoad: true,
      navText: ['<span><i class="fi flaticon-caret-left"></i><em class="hidden">Previous</em></span>',
      '<span><i class="fi flaticon-caret-right"></i><em class="hidden">Next</em></span>'],
      responsive: {
        0: {
          items: 1
        },
        992: {
          items: 3
        },
      }
    });

    // Tour Box carousels init
    var tourBoxesCarousel = $('.tour-box-img-carousel');
    tourBoxesCarousel.owlCarousel({
      items:1,
      loop:false,
      dots: true,
      nav: true,
      lazyLoad: true,
    // onTranslate: function(event) {
	//			//REVALIDATE
	//			console.log("Revalidate blazy")
	//			lazyLoadInstance.update();
	//		},
      navText: ['<span><img class="owlNavArrows" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
    });


    // Tour Results Box carousels init
    var tourResultsBoxesCarousel = $('.tour-results-box-img-carousel');
    // console.log("testing lazy with owl");
    tourResultsBoxesCarousel.owlCarousel({
      items:1,
      loop:false,
      dots: true,
      nav: true,
      lazyLoad: true,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},

      // navText: ['<span><img class="owlNavArrows" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
      navText: ['<span><i class="owlNavArrows fi flaticon-caret-left"></i></span>', '<span><i class="owlNavArrows fi flaticon-caret-right"></i></span>'],
    });

    // Three quarters layout carousel init
    var threeQuartersCarousel = $('.full-width-carousel-with-side-content .owl-carousel');
    threeQuartersCarousel.owlCarousel({
      items:1,
      loop:false,
      dots: true,
      nav: true,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},
      navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
    });

    var checkWidth = window.innerWidth;
    var accommodationsSectionCarousel = $('#owl-accom-slider');
    //accommodationsSectionCarousel.addClass('owl-carousel');
    if(checkWidth >= 992){
	  accommodationsSectionCarousel.owlCarousel({
	    items: 1,
	    loop:false,
	    dots: false,
	    nav: false,
	    autoHeight:true,
	    onTranslate: function(event) {
	      //REVALIDATE
	      lazyLoadInstance.update();
	    },
	  });
	  $('.itinerary-accommodations-nav.prev').on('click', function(e){
		  e.preventDefault();
		  accommodationsSectionCarousel.trigger('prev.owl.carousel');
	  });

	  $('.itinerary-accommodations-nav.next').on('click', function(e){
		  e.preventDefault();
		  accommodationsSectionCarousel.trigger('next.owl.carousel');
	  });
    }else{
	   //destroy owl-carousel and remove all depending classes if window screensize is smaller then 767px
	   accommodationsSectionCarousel.trigger('destroy.owl.carousel').removeClass('owl-carousel owl-loaded');
	   accommodationsSectionCarousel.find('.owl-stage-outer').children().unwrap();
    }

    // Accommodations Day Individual Carousel
    var accommodationsDayCarousel = $('.accommodation-slider-day .owl-carousel');
    accommodationsDayCarousel.owlCarousel({
      items:1,
      loop:false,
      dots: true,
      nav: true,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},
      navText: ['<span><i class="fi flaticon-caret-left"></i><em class="hidden">Previous</em></span>',
          '<span><i class="fi flaticon-caret-right"></i><em class="hidden">Next</em></span>'],
    });

    // Region Tour Box Carousel
    var regionBoxesCarousel = $('.carousel-boxes-wrap');

    var destcount = $('.carousel-boxes-wrap').find(".carousel-box").length;

    if (destcount > 3) {
        regionBoxesCarousel.owlCarousel({
          items:4,
          margin: 36,
          loop:true,
          dots: false,
          nav: false,
		      onTranslate: function(event) {
						//REVALIDATE
						lazyLoadInstance.update();
					},
          // navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
          responsiveClass:true,
          responsive:{
            0:{
              items:1,
              slideBy: 1,
              margin: 0,
              center: true
            },
            640:{
              items:2,
              slideBy: 2,
            },
            992:{
              items:3,
              slideBy: 3,
            },
            1199:{
              items:4,
              slideBy: 4,
            }
          }
        });
    } else {

        $(".carousel-boxes-nav").addClass("hidenav" + destcount);

        regionBoxesCarousel.owlCarousel({
            items: destcount,
            margin: 36,
            loop: false,
            dots: false,
            nav: false,
			      onTranslate: function(event) {
							//REVALIDATE
							lazyLoadInstance.update();
						},
            // navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    slideBy: 1,
                    margin: 0,
                    center: true
                },
                640:{
                    items:2,
                    slideBy: 2,
                },
                992:{
                    items:3,
                    slideBy: 3,
                },
                1199:{
                    items:4,
                    slideBy: 4
                }
            }
        });

    }


    $('.carousel-boxes-nav-prev').on('click', function(){
       regionBoxesCarousel.trigger('prev.owl.carousel');
    });

    $('.carousel-boxes-nav-next').on('click', function(){
       regionBoxesCarousel.trigger('next.owl.carousel');
    });


    // Three quarters layout carousel init
    var overflowCarousel = $('.overflow-carousel');
    overflowCarousel.owlCarousel({
      items: 1,
      slideBy: 1,
      margin: 36,
      loop:true,
      nav: true,
      dots: false,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},
      navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
      responsiveClass:true,
      responsive:{
        0:{
          items:1,
          slideBy: 1,
          stagePadding: 0,
        },
        767:{
          items:2,
          slideBy: 1,
          stagePadding: 0,
        },
        992:{
          items:2,
          slideBy: 1,
          stagePadding: 80,
        },
        1199:{
          items:1,
          slideBy: 1,
          stagePadding: 80,
        },
        1299:{
          items:2,
          slideBy: 1,
          stagePadding: 80,
        }
      }
    });

    // Three quarters layout carousel init
    var guidesCarousel = $('.guides-carousel');

    var guidecount = $('.guides-carousel').find(".guide-box").length;

    guidesCarousel.owlCarousel({
      items: 1,
      slideBy: 1,
      margin: 36,
      loop:true,
      nav: true,
      dots: false,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},
      navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
      responsiveClass:true,
      responsive:{
        0:{
          items:1,
          slideBy: 1,
          margin: 0,
        },
        767:{
          items:2,
          slideBy: 1,
        },
        992:{
          items:2,
          slideBy: 1,
        },
        1199:{
          items:3,
          slideBy: 1,
        },
        1299:{
          items:3,
          slideBy: 1,
        }
      }
    });

    // Biking itinerary carousel
    var bikingCarousel = $('.biking-itinerary-carousel');
    bikingCarousel.owlCarousel({
      items: 1,
      slideBy: 1,
      // margin: 36,
      loop:true,
      nav: true,
      dots: false,
      onTranslate: function(event) {
				//REVALIDATE
				lazyLoadInstance.update();
			},
      navText: ['<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows svg" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
    }); 



  };


  /* ========================================================================
   * Tour Box Favorite Functionality
   * ======================================================================== */

  CMDB.tourBoxFavorite = function () {

    if ($('.tour-box-favorite').length) {

        $('.tour-box-favorite').each(function () {
            var current_favorites = getStorageItem( 'favorites' );
            var post_id = parseInt($(this).data('post-id'));

            if( !current_favorites ) {
                current_favorites = [];
            }

            if (current_favorites.indexOf(post_id) !== -1) {
                $(this).find('.fi').addClass('active');
            } else {
                $(this).find('.fi').removeClass('active');
            }
        });

    }
  };


    /* ========================================================================
     * Tour Box Compare Functionality - checks boxes on load that are in compare list
     * ======================================================================== */

    CMDB.tourBoxCompare = function () {

        if ($('.compare-tour').length) {

            $('.tour-box-content .compare-tour').each(function () {
                var current_compare = getStorageItem( 'compare' );
                var post_id = parseInt($(this).data('post-id'));

                if( !current_compare ) {
                    current_compare = [];
                }

                if (current_compare.indexOf(post_id) !== -1) {
                    $(this).find('.custom-control-input').prop('checked', true);
                } else {
                    $(this).find('.custom-control-input').prop('checked', false);
                }
            });

        }
    };


  /* ========================================================================
   * Booking Widget - Date Range
   * ======================================================================== */

  CMDB.setupDateRange = function () {
      // departure exists in the DOM. Continue.

      var departureinput  = $('#bkn_departure');
      var returninput     = $('#bkn_return');

      if ( departureinput.length && returninput.length ) {
          // get current time.
          var today = new Date();
          var departdate = departureinput.val();
          var returndate = returninput.val();
          var drops;

          if( $(window).width() > 1200 ) {
              drops = "up";
          } else {
              drops = "";
          }

          // set attributes on timer. Have date and year drop downs. Single item and format to year month day.
          // do the same for the second calendar.
          var attr = {
              "singleDatePicker": true,
              "showDropdowns": true,
              "locale": {
                  "format" : "YYYY-MM-DD"
              },
              "startDate" : departdate,
              "minDate": today,
              "maxDate": today.getFullYear() + 1,
              "maxYear" : today.getFullYear() + 1,
              "parentEl" : ".banner",
              "drops" : drops,
          }

          var attr2 = {
              "singleDatePicker": true,
              "showDropdowns": true,
              "locale": {
                  "format" : "YYYY-MM-DD"
              },
              "startDate" : returndate,
              "minDate" : today,
              "maxDate" : today.getFullYear() + 1,
              "maxYear" : today.getFullYear() + 1,
              "parentEl" : ".banner",
              "drops" : drops,
          }

          departureinput.daterangepicker(attr, function(start,end,label){
              start.format('YYYY-MM-DD');
          });


          // setup cross calendar options. Sets the min date automatically on the second calendar.
          departureinput.on('apply.daterangepicker', function(ev, picker) {
              //picker.startDate.format('YYYY-MM-DD');
              returninput.data('daterangepicker').setStartDate(picker.startDate.format('YYYY-MM-DD'));
              returninput.data('daterangepicker').setEndDate(picker.startDate.format('YYYY-MM-DD'));
              returninput.data('daterangepicker').minDate = picker.startDate;
          });

          returninput.on('apply.daterangepicker', function(ev, picker) {
              //picker.startDate.format('YYYY-MM-DD');
              departureinput.data('daterangepicker').maxDate = picker.startDate;

          });
          returninput.daterangepicker(attr2);
      }
  };

  /* ========================================================================
   * Booking Widget - Custom Drop Downs Setup
   * ======================================================================== */

  CMDB.customDropSetup = function(){
      var customselect = $(".custom-select");
      // convert select to simply ul element with a wrapper
      if (customselect.length) {
          customselect.each(function(i, n){
              var $this = $(this);
              var inputname = $this.attr("name");
              var inputid = $this.attr("id");
              var inputval = $this.data("value");
              // build hidden input to keep track of user selection easily
              $this.after('<input type="hidden" name="'+inputname+'" value="'+inputval+'" id="' + inputid + '">' );

              // build out list element to more easily manipulate.
              var list = '<div class="wrapper-dropdown">';

              // get first child to use as the default item
              var defaulttext = $this.find('option').first().text();
              list += '<span>' + defaulttext + '</span>';

              list += '<ul class="dropdown">';
              $this.find('option').each(function(i, x){
                // check for other city option
                if ($(this).val() == 'otherCity'){
                  list += '<li class="other-city"><a href="#" data-value="'+$(this).val() +'" class="other-city">'+ $(this).text() +'</a></li>';
                }
                else{
                  list += '<li><a href="#" data-value="'+$(this).val() +'">'+ $(this).text() +'</a></li>';
                }
              });
              list += '</ul>';
              list += '</div>';
              $this.after(list);
              $this.remove();

          });
      }

  };

  /* ========================================================================
   * Booking Widget - Custom Drop Downs Handler
   * ======================================================================== */

  CMDB.customDropHandler = function(){

      // Unbind the event first as we need to re-enable it when we reload the dropdowns through an ajax call
      // The reload would attach this event to the old buttons for a second time - Which breaks the old buttons
      $(".wrapper-dropdown").unbind('click').click(function() {
        //console.log('unbind dropdown click event');
      });

      $(".wrapper-dropdown").on('click', function(evt){
          $(this).parent("div").toggleClass('active');
          evt.stopPropagation();
      });

      $(".wrapper-dropdown ul li a").on('click', function(evt){
          evt.preventDefault();
          var selected = $(this).text();
          var selectvalue = $(this).data('value');
          $(this).parent("li").parent("ul").prev("span").text(selected);

          var input = $(this).parent("li").parent("ul").parent(".wrapper-dropdown").next("input");

          input.val(selectvalue);
          input.trigger("change");

      });

  };

  /* ========================================================================
   * Booking Widget - Custom Destination Picker
   * ======================================================================== */

	CMDB.customDestinationPicker = function(){
		if ($('.header-booking-widget').length) {

            $document.mouseup(function(e) {
                var container = $('.floating-container.active');

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                    $('.booking-widget-wrapper .icon.active').removeClass('active');
                    $('.booking-widget-input.active').removeClass('active');
                }
            });

			// Destination Picker menu
			$('.booking-widget-destination-picker').on('click', function(){
				var destinationsHolder = $(this).parents('.booking-widget-row').find('.destinations-holder');
				$('#booking-widget-travel-style-picker').removeClass('active');
				$('#booking-widget-travel-style-picker').parent().removeClass('active');
				$('#travel-styles-holder').removeClass('active');
				$('#travel-styles-holder').removeClass('hide');
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).parent().removeClass('active');
					destinationsHolder.hide();
					destinationsHolder.removeClass('active');
				}else{
					$(this).addClass('active');
					$(this).parent().addClass('active');
					destinationsHolder.show();
					destinationsHolder.addClass('active');
				}
			})

			// Copied from megamenu setup, kinda duplicated, but hey...
			// Automatic menu builder for post type hirerarchy setup
			// Get the first menu list and show it's menu items
	    $('.bw-destinations-left-link').first().addClass('active');
	    var activeID = $('.bw-destinations-left-link.active').attr('data-destination-id');
	    $('.bw-destinations-right .region-box[data-region-id="'+activeID+'"]').show();

			// sub menu menu system data attributes setup
	    $('.bw-destinations-left-link').each(function(index, el) {
	    	var theID = $(this).attr('data-destination-id');
	    	var theURL = $(this).attr('data-url');
	    	var theTitle = $(this).attr('data-title');
	    	$('.bw-destinations-right .region-box[data-region-id="'+theID+'"] .region-box-header').append('<a class="region-box-header-link" href="'+theURL+'">Explore '+theTitle+'</a>');
	    });

	    // sub menu menu system toggle
	    $('.bw-destinations-left .bw-destinations-left-link').on('click', function(e){
	    	e.preventDefault();
	    	var theID = $(this).data('destination-id');
	    	if (!isMobile()) {
		    	$('.bw-destinations-right .region-box').removeClass('active').hide();
		    	$('.bw-destinations-left-link').removeClass('active');
		    	$(this).addClass('active');
		    	$(".bw-destinations-right .region-box[data-region-id='"+theID+"']").addClass('active').show();
	    	}else{
	    		// var theURL = $(this).attr('data-url');
	    		// window.location.href = theURL;
	    	}
	    })
	    $('.bw-destinations-right .region-link').on('click', function(e){
	    	e.preventDefault();
	    	var theDestination = $(this).attr('data-destination-name');
	    	$('.booking-widget-destination-picker').val(theDestination);
	    	$('.booking-widget-destination-picker').parent().removeClass('active');
				$('.booking-widget-destination-picker').removeClass('active');
				$('.booking-widget-destination-picker').parents('.booking-widget-row').find('.destinations-holder').hide();
                if (window.innerWidth < 768) {
                    $('html, body').animate({scrollTop: $(".header-booking-widget").offset().top}, 500);
                }
	    });


			// Travel Styles Menu
			$('.booking-widget-travel-style-picker').on('click', function(){
				$('#booking-widget-destination-picker').removeClass('active');
				$('#booking-widget-destination-picker').parent().removeClass('active');
				$('#destinations-holder').removeClass('active');
				$('#destinations-holder').removeClass('hide');
				var travelStylesHolder = $(this).parents('.booking-widget-row').find('.travel-styles-holder');
				if ($(this).hasClass('active')) {
					$(this).removeClass('active');
					$(this).parent().removeClass('active');
					travelStylesHolder.hide();
					travelStylesHolder.removeClass('active');
				}else{
					$(this).addClass('active');
					$(this).parent().addClass('active');
					travelStylesHolder.show();
					travelStylesHolder.addClass('active');
				}
			})

		  $('.travle-styles-picker input').on('click', function(e){
		    if ($(this).is(":checked")) {
		      $(this).addClass('is-checked');
		    }else{
		      $(this).removeClass('is-checked');
		    }
		  });

		}
	}

/* ========================================================================
 * TabCordion
 * ======================================================================== */

  CMDB.tabcordion = function () {

      // function getParameterByName(name) {
      //   name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      //   var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      //     results = regex.exec(location.search);
      //   return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
      // }

      var accordiontabs = "accordion";
      $(".tabcordion").each(function() {
        if (!$("body").hasClass("js")) $("body").addClass("js");

        var tab = getParameterByName('tab');
        var numTabs = $(this).find(".heading").length;

        // Don't do this on the dates and prices table
        if (!$(this).hasClass("dates-prices-tabcordion")) {
          if (tab.length > 0 && tab > 0 && tab <= numTabs) {
            $(this).find(".active-heading").removeClass("active-heading");
            $(this).find(".heading").eq((tab) - 1).addClass("active-heading");
            $(this).find(".content").eq((tab) - 1).addClass("active-content").css('display', 'block');
          } else {
            $(this).find(".heading").first().addClass("active-heading");
            $(this).find(".content").first().addClass("active-content").css('display', 'block');
          }
        } else {
          // On the dates and prices table we just want to display the active content table content
          $(this).find(".active-content").css('display', 'block');
        }


        if (window.innerWidth > 767) {
          $(this).prepend('<div class="tab-headings"></div>');
          $(this).find(".heading").appendTo($(this).find(".tab-headings"));
          accordiontabs = "tabs";
          $('.tab-headings').wrap('<div class="tab-headings-wrapper"></div>');
// Added
          if($(this).find('.tab-group-title').length){
            $(this).find('.tab-headings').prepend($(this).find('.tab-group-title'));
          }
          if($(this).find('.tab-group-end').length){
            $(this).find('.tab-headings').append($(this).find('.tab-group-end'));
          }
// End
        }else{
// Added
          if($(this).find('.tab-group-title').length){
            $(this).find('.tabcordion').prepend($(this).find('.tab-group-title'));
          }
          if($(this).find('.tab-group-end').length){
            $(this).find('.tabcordion').prepend($(this).find('.tab-group-end'));
          }
// End
        }
      });
      $(".tabcordion .heading").click(function() {
        if (window.innerWidth < 768) {
          if (!$(this).hasClass("active-heading")) {
            $(this).parents('.tabcordion').find(".active-heading").removeClass("active-heading");
            $(this).addClass("active-heading");
            $(this).parents('.tabcordion').find(".active-content").slideToggle().removeClass("active-content in");
            $(this).next(".content").addClass("active-content in").slideToggle();
          } else {
            $(this).parents('.tabcordion').find(".active-heading").removeClass("active-heading");
            $(this).next(".content").removeClass("active-content").slideToggle();
          }
        } else {
          if (!$(this).hasClass("active-heading")) {
            $(this).parents('.tabcordion').find(".active-heading").removeClass("active-heading");
            $(this).addClass("active-heading");
            $(this).parents('.tabcordion').find(".active-content").css('display', 'none').removeClass("active-content");
            var tab = $(this).attr('id').replace("heading-", "");
            $(this).parents('.tabcordion').find("#content-" + tab).addClass("active-content in").fadeIn();
          }
        }

      });
      $(window).resize(function() {
        if (window.innerWidth < 768) {
          if (accordiontabs != "accordion") {
            $(".tabcordion").each(function() {
              $(this).find(".heading").each(function() {
                var tab = $(this).attr('id').replace("heading-", "");
                $(this).insertBefore("#content-" + tab);
              });
// Added
              if($(this).find('.tab-group-title').length){
                $(this).prepend($(this).find('.tab-group-title'));
              }
              if($(this).find('.tab-group-end').length){
                $(this).append($(this).find('.tab-group-end'));
              }
// End
              $(this).find(".tab-headings").remove();
              $(this).find('.tab-headings-wrapper').remove();
            });
            accordiontabs = "accordion";
          }
        } else {
          if (accordiontabs != "tabs") {
            $(".tabcordion").each(function() {
              $(this).prepend('<div class="tab-headings"></div>');
              $(this).find(".heading").appendTo($(this).find(".tab-headings"));
              if ($(this).find(".active-heading").length < 1) {
                $(this).find(".heading").first().addClass("active-heading");
                $(this).find(".content").first().addClass("active-content").css('display', 'block');
              }
              $(this).find('.tab-headings').wrap('<div class="tab-headings-wrapper"></div>');
// Added
              if($(this).find('.tab-group-title').length){
                $(this).find('.tab-headings').prepend($(this).find('.tab-group-title'));
              }
              if($(this).find('.tab-group-end').length){
                $(this).find('.tab-headings').append($(this).find('.tab-group-end'));
              }
// End
            });
            accordiontabs = "tabs";
          }
        }
      });


    };


/* ========================================================================
 * END TabCordion
 * ======================================================================== */



/* ========================================================================
 * Itinerary Page Stuff
 * ======================================================================== */

  /* ========================================================================
   * Itinerary Page Nav
   * ======================================================================== */

  CMDB.itineraryPagePopup = function () {
  	if ($('#activityChartModal').length) {
			$('#activityChartModal').on('show.bs.modal', function (e) {
			  // Do something here with the title or content before the popup is displayed
			})
  	}
  }

  CMDB.itineraryPagePerPostCollapseBox = function () {
  	if ($('.prePostCollapse').length) {
  		$('.prePostCollapse').on('click', function(e){
  			e.preventDefault();
  			if ($(this).parents('.extensions-box-content').find('.prePostContentBox').hasClass('show')) {
	  			$(this).parents('.extensions-box-content').find('.prePostContentBox').collapse('hide');
	  			$(this).find('.view-toggle').html('View');
  			}else{
  				$(this).parents('.extensions-box-content').find('.prePostContentBox').collapse('show');
  				$(this).find('.view-toggle').html('Hide');
  			}
  			// $($(e.target).data('bs.collapse')._triggerArray[0]).parents('.extensions-box-content').find('.view-toggle').html('View');
  		})
  	}
  }

  CMDB.itineraryExtensionYearSwitch = function () {
    if ($('.extension-year-switch').length) {
      $('.extension-year-switch').on('click', function (e) {
        e.preventDefault();

        // Show the active year extension box
        var extensionyear = $(this).data("extensionyear");
        $(this).closest('.extension-wrapper').find('.extensions-box').removeClass('active-extension');
        $(this).closest('.extension-wrapper').find('.extension-'+extensionyear).addClass('active-extension');

        // Hide everything and then only show the first date price
        
        // Pre data reset prices to first price so something shows up on the year switch
        $('.pre_ext_pricing').hide();
        $('.extensions-box').each(function(index) {
          $(this).find('.pre_ext_pricing').first().show();
        });
        
        // Post data reset prices to first price so something shows up on the year switch
        $('.post_ext_pricing').hide();
        $('.extensions-box').each(function(index) {
          $(this).find('.post_ext_pricing').first().show();
        });

        // Reset itinerary dropdown as the extension prices will no longer match
        $('.itinerary-select-wrapper > .wrapper-dropdown > span').text('Next Departure');
      })
    }
  }

  CMDB.itineraryPageNav = function () {

    if ($('.itinerary-nav-menu').length) {
      $('.itinerary-nav-link').on('click', function(e){
        var link = $(this).attr('href');
        $('.itinerary-nav-menu .heading a').removeClass('active');
        $(this).addClass('active')
        $('html, body').animate({
            scrollTop: $(link).offset().top - 80
        }, 600);
        e.preventDefault();
      })
    }

    $('body').scrollspy({
		  target: '#itinerary-nav-menu',
		  // offset: 145
		  offset: $(window).innerHeight() / 2
		});

  }

    /* ========================================================================
   * Itinerary Dates and Prices Table
   * ======================================================================== */

// The idea is to rearrange the markup in the big table to accomodate a new 'tab layout' on mobile
  CMDB.itineraryDatesAndPricesTable = function() {
    if ($('#itinerary-dates-prices').length) {
      var setupForMobile = false;
      if (window.innerWidth < 768) {
          // Transition to Mobile
          convertToMobile();
          setupForMobile = true;
      }
      $(window).resize(function() {

        if (window.innerWidth < 768) {
          if (setupForMobile == false) {
            // Transition to Mobile
            convertToMobile();
            setupForMobile = true;
          }
        } else {
          if (setupForMobile == true) {
            // Transition to Desktop
            convertToDesktop();
            setupForMobile = false;
          }
        }

      });
    }
  }
  function convertToMobile(){
    $('.dates-prices-tabcordion .pricing-table').each(function() {
      var tabAirSide = $(this).find('.table-box-tab-air');
      var tabLandSide = $(this).find('.table-box-tab-land');
      $(this).find('.table-box-tab-wrapper .table-box').each(function() {
        $(this).find('.air-side').appendTo(tabAirSide);
        $(this).find('.land-side').appendTo(tabLandSide);
      });
      $(this).find('.table-box.top .air-side').prependTo(tabAirSide).addClass('top');
      $(this).find('.table-box.top .land-side').prependTo(tabLandSide).addClass('top');
      $(this).find('.table-box-tab-wrapper').empty();
      // Toggle first accordion to be open
      $(this).find('.nav-link').removeClass('active');
      $(this).find('.nav-link').first().addClass('active');
      $(this).find('.table-box').removeClass('active show');
      $(this).find('.table-box').first().addClass('active show');
    });
  }
  function convertToDesktop(){
    $('.dates-prices-tabcordion .pricing-table').each(function() {

    	var thisTable = $(this);
      var airSide = [];
      var landSide = [];

      $(this).find('.table-box.tab-pane .air-side.data-box').each(function() {
        airSide.push({
          line: $(this).data('line'),
          air: $(this),
        });
      });
      $(this).find('.table-box.tab-pane .land-side.data-box').each(function() {
        landSide.push({
          line: $(this).data('line'),
          land: $(this),
        });
      });

      var recompiledRows = [];

      $.each(landSide, function(index, el) {
        recompiledRows.push(Object.assign({}, el, airSide[index]));
      });

      $.each(recompiledRows, function(index, el) {
        var airElement;
        if ($(el.air).length) {
          airElement = el.air[0];
        }
        var landElement;
        if ($(el.land).length) {
          landElement = el.land[0];
        }
        var tableBoxEl = $('<div class="table-box"></div>');
        var tableWrapper = $(thisTable).find('.table-box-tab-wrapper').append(tableBoxEl);
        if ($(airElement).length > 0) {
          tableBoxEl.append(airElement);
        }
        if ($(landElement).length > 0) {
          tableBoxEl.append(landElement);
        }
      });
      $(thisTable).find('.table-box.top').append($(this).find('.table-box-tab-air .top'));
      $(thisTable).find('.table-box.top').append($(this).find('.table-box-tab-land .top'));
    });
  }



  /* ========================================================================
   * END Itinerary Page Nav
   * ======================================================================== */




  /* ========================================================================
   * Itinerary Collapse Outside of region
   * ======================================================================== */


	CMDB.itineraryCollapseOutsideReigion = function(){
		if ($('.itinerary-accordion').length) {
			$(document).click(function(e) {
				if ($(e.target).parents('.itinerary-accordion').length || $(e.target).hasClass('expand-all-toggle') || $(e.target).hasClass('itinerary-expand-all-toggle')) {
					return false;
				}else{
					$('.itinerary-accordion .collapse.show').collapse('hide');
					if ($('.expand-all-toggle').length) {
						$('.expand-all-toggle').html('Expand All <span class="plus">+</span>');
						$('.expand-all-toggle').removeClass('collapsed');
					};
					if ($('.itinerary-expand-all-toggle').length) {
						$('.itinerary-expand-all-toggle').html('Expand All <span class="plus">+</span>');
						$('.itinerary-expand-all-toggle').removeClass('collapsed');
					};
				}
			});
		}
	}


  /* ========================================================================
   * Itinerary Collapse Toggle
   * ======================================================================== */


	CMDB.itineraryCollapseToggle = function(){
		if ($('.itinerary-expand-all-toggle').length) {
			$('.itinerary-expand-all-toggle').on('click', function(e){
				e.preventDefault();
				if ($('.itinerary-expand-all-toggle').hasClass('collapsed')) {
					$(this).parents('.itinerary-accordion').find('.collapse').collapse('hide');
					$('.itinerary-expand-all-toggle').html('Expand All <span class="plus">+</span>');
					$('.itinerary-expand-all-toggle').removeClass('collapsed');
				}else{
					$(this).parents('.itinerary-accordion').find('.collapse').collapse('show');
					$('.itinerary-expand-all-toggle').html('Collapse All <span class="plus">-</span>');
					$('.itinerary-expand-all-toggle').addClass('collapsed');
				}
			})
		}
	}


  /* ========================================================================
   * Biking Itinerary Collapse Toggle
   * ======================================================================== */


	CMDB.bikingItineraryCollapseToggle = function(){
		if ($('.expand-all-toggle').length) {
			$('.expand-all-toggle').on('click', function(e){
				e.preventDefault();
				if ($('.expand-all-toggle').hasClass('collapsed')) {
					$('.biking-itinerary-accordion .collapse').collapse('hide');
					$('.expand-all-toggle').html('Expand All <span class="plus">+</span>');
					$('.expand-all-toggle').removeClass('collapsed');
				}else{
					$('.biking-itinerary-accordion .collapse').collapse('show');
					$('.expand-all-toggle').html('Collapse All <span class="plus">-</span>');
					$('.expand-all-toggle').addClass('collapsed');
				}
			})
		}
	}


  CMDB.basicAccordianToggle = function(){
    if ($('.expand-all-basic').length) {
      $('.expand-all-basic').on('click', function(e){
        e.preventDefault();
        if ($('.expand-all-basic').hasClass('collapsed')) {
          // console.log(this);

          $(this).parents('.basic-accordion-container').find('.collapse').collapse('hide');
          $(this).parents('.basic-accordion-container').find('.expand-all-basic').html('Expand All <span class="plus">+</span>');
          $(this).parents('.basic-accordion-container').find('.expand-all-basic').removeClass('collapsed');

          // $('.content-accordion .collapse').collapse('hide');
          // $('.expand-all-basic').html('Expand All <span class="plus">+</span>');
          // $('.expand-all-basic').removeClass('collapsed');
        }else{
          // $('.content-accordion .collapse').collapse('show');
          $(this).parents('.basic-accordion-container').find('.collapse').collapse('show');
          $(this).parents('.basic-accordion-container').find('.expand-all-basic').html('Collapse All <span class="plus">-</span>');
          $(this).parents('.basic-accordion-container').find('.expand-all-basic').addClass('collapsed');

          // $('.expand-all-basic').html('Collapse All <span class="plus">-</span>');
          // $('.expand-all-basic').addClass('collapsed');
          // console.log('HasClass');
        }
      })
    }
  }



	CMDB.callLoadMore = function(options){

        var offset = 0;
        var button = null;
        var btntext = null;
        var currentcount = 0;
        var totalcount = 0;
        var hideme = jQuery(".hideme");
        var button = jQuery(".load-more");
        var btntext = button.text();

        button.text('Loading');
        button.addClass("loading");



        // grab button attributes which override any other settings.
        var params = jQuery("a.load-more").data();

        jQuery.each(params, function(key, value) {
            options[key] = value;
        });

        var container = button.closest('section').prev('section').find('.container');
        if (!container.data('currentoffset')) {
            // loop through for .cta class
            // var count = container.find(".cta");
            var count = container.find("div[data-postloader='postloader']");
            offset = container.data('currentoffset', count.length);
        } else {
            offset = container.data('currentoffset');
        }
        offset = container.data('currentoffset');

        jQuery.post(
            postloader.ajaxurl,options,
            function(response) {
                // var current = $(".cards .container");
                var current = container;
                offset = parseInt(offset) + 5;
                container.data('currentoffset', offset);
                response = jQuery.trim(response);
                response = jQuery(jQuery.parseHTML(response));
                response.addClass("loading");
                current.append(response);
                var newkids = current.find(".loading");
                newkids.css({'display':'none'});
                newkids.fadeIn(500);

                // check to see what our count is at.
                if (offset >= response.data('total')) {
                    //Hide the button when no other blogs can be loaded
                    button.css('visibility', 'hidden');
                    button.text("No more results");
                    button.addClass("done");
                    if (response.data('total') > 0) {
                        jQuery(".totalcount").text(response.data('total'));
                    }
                } else {
                    jQuery(".totalcount").text(response.data('total'));
                    button.text(btntext);
                    button.css('visibility', 'visible');
                }
                var ctas = current.find(".cta");
                jQuery(".currentcount").text(ctas.length);
                newkids.removeClass('loading');
                if (response.data('total') > offset ) {
                    button.text("View more results");
                }

                button.removeClass("loading");
                hideme.removeClass("hideme");
                lazyLoadInstance.update();
                addthis.layers.refresh();
            }
        );

        return true

	}


    CMDB.setupBlogListing = function() {

        var bloglisting = $(".article-finder");
        if (bloglisting.length) {
            var options = {
                'action'        : 'postlookup',
                'type'          : 'post',
                'count'         : '5',
                'offset'        : 0,
                'design'        : 'html',
                'orderby'       : 'date',
                'order'         : 'DESC',
                'displaystyle'  : 'two',
                'category'      : blogcategory //delcared in the markup home.php
            }
            CMDB.callLoadMore(options);
        }
    }




    CMDB.setupBlogClick = function() {

        jQuery('a.btn.btn-secondary.load-more').on('click', function(evt) {

            var offset = jQuery(".currentcount").text();

            //postoptions.filter.offset = offset; // new
            evt.preventDefault();
            var options = {
                'action'        : 'postlookup',
                'type'          : 'post',
                'count'         : '5',
                'offset'        : offset,
                'design'        : 'html',
                'orderby'       : 'date',
                'order'         : 'DESC',
                'displaystyle'  : 'two',
            }
            CMDB.callLoadMore(options);
        });

    }


    CMDB.blogHideItem = function(options) {
        var elm = jQuery(options);
        var childs = elm.find(".cta");

        //if (elm.length > 0) {

        //var op = 1;  // initial opacity
        //    var timer = setInterval(function () {
        //        if (op <= 0.1){
        //            clearInterval(timer);
                    elm.empty();
        //            return;
        //        }
        //        childs.css({'opacity' : op});
        //        op -= op * 0.1;
        //    }, 20);
        //}

        return;
    }


    /* ========================================================================
     * Adds listening for input fields since an input needs to be emptied then
     * replaced for dynamic js to detect it. We also reset the listing here as
     * well, since it will be a new listing of items after a filter.
     * ======================================================================== */

    CMDB.setupChangeEvents = function() {


        var artcategory = $('#article-find-by-category');
        var artsort = $('#article-sort-by');

        artcategory.each(function() {
            var elem = $(this);

            // Save current value of element
            elem.data('oldVal', elem.val());

            // Look for changes in the value
            elem.bind("propertychange change click keyup input paste", function(event){
                // If value has changed...
                if (elem.data('oldVal') != elem.val()) {
                    elem.data('oldVal', elem.val());
                    var options = {
                        'action' : 'postlookup',
                        'type' : 'post',
                        'count' : '5',
                        'offset' : 0,
                        'category' : elem.val(),
                        'design' : 'html',
                        'displaystyle' : 'two',
                        'orderby'       : 'date',
                        'order'         : artsort.val(),
                    }
                    // also want to add the category to the data attributes to its tracked.
                    jQuery("a.load-more").data('category', elem.val());
                    jQuery(".blog-listings.start>div.container").data('currentoffset', 0);
                    CMDB.blogHideItem(".blog-listings.start>div.container");
                    CMDB.callLoadMore(options);
                }
            });
        });

        artsort.each(function() {
            var elem = $(this);

            // Save current value of element
            elem.data('oldVal', elem.val());

            // Look for changes in the value
            elem.bind("propertychange change click keyup input paste", function(event){
                // If value has changed...
                if (elem.data('oldVal') != elem.val()) {
                    elem.data('oldVal', elem.val());
                    var options = {
                        'action' : 'postlookup',
                        'type' : 'post',
                        'count' : '5',
                        'offset' : 0,
                        'category' : artcategory.val(),
                        'design' : 'html',
                        'displaystyle' : 'two',
                        'orderby'       : 'date',
                        'order'         : elem.val(),
                    }
                    // also want to add the category to the data attributes to its tracked.
                    jQuery("a.load-more").data('category', elem.val());
                    jQuery(".blog-listings.start>div.container").data('currentoffset', 0);
                    CMDB.blogHideItem(".blog-listings.start>div.container");
                    CMDB.callLoadMore(options);
                }
            });
        });
    }

    CMDB.setupBlogSearch = function() {

        var searchbar = jQuery("#blogsearch");
        searchbar.on('change, keyup', function(evt){
            var search = searchbar.val();
            var button = jQuery(".load-more");
            var working = false;
            working = button.hasClass("loading");

            // only fire the search if the listing is bigger than 3.
            if ( (search.length > 3) && (working == false) ) {
                var options = {
                    'action' : 'postlookup',
                    'type' : 'post',
                    'count' : '5',
                    'offset' : 0,
                    'search' : searchbar.val(),
                    'design' : 'html',
                    'displaystyle' : 'two'
                }
                jQuery("a.load-more").data('search', searchbar.val());
                jQuery(".blog-listings.start>div.container").data('currentoffset', 0);
                CMDB.blogHideItem(".blog-listings.start>div.container");
                CMDB.callLoadMore(options);
            }
        });
    }


/* ========================================================================
    Tour JSON Call
 */
var submitting = false;
var pagedError = false;
var currentRequest = null;

CMDB.tourJSON = function() {


    if (submitting == true) {
        currentRequest.abort();
    }
    // hide results.
    $(".tour-boxes").fadeOut();
    submitting = true;

    // ajax data will be passed to this.
    var pagecontent = {};
    var container = jQuery(".tour-results-boxes.tour-boxes");

    // build request

    var getdata = {};

    // setup destinations
    var destinations = jQuery(".destination-filter-box input:checked").map(function(){
        return this.value;
    }).get().join("-");

    // setup tourtypes
    var tourtype = jQuery(".filter-box-content-inner .tour-type-list.biking-walking input:checked").map(function(){
        return this.value;
    }).get().join("-");

    var activity = jQuery(".activity-level-filter-box .activity-level-list input:checked").map(function(){
        return this.value;
    }).get().join("-");

    var newtour     = jQuery("input[name=new]:checked").val();
    var deals       = jQuery("input[name=deals]:checked").val();
    var sortby      = jQuery("input[name=sort-by]").val();
    var minprice    = jQuery("input[name=price-range-low]").val();
    var maxprice    = jQuery("input[name=price-range-high]").val();
    var paged       = jQuery(".pagination").data('paged');
    var tourext     = jQuery("input[name=tourext]:checked").val();
    var bikeair     = jQuery("input[name=bikeair]:checked").val();
    var bikeonly    = jQuery("input[name=bikeonly]:checked").val();
    var returndate  = jQuery("input[name=return]").val();
    var departdate  = jQuery("input[name=departure]").val();

    //This checks to see if right number of paged is available in the new filter search.
    if (pagedError == true){
      paged = 1;
    }


    getdata = {
        'destination'   : destinations,
        'tourtype'      : tourtype,
        'activity'      : activity,
        'new'           : newtour,
        'deals'         : deals,
        'order'         : sortby,
        'minprice'      : minprice,
        'maxprice'      : maxprice,
        'hasext'        : tourext,
        'paged'         : paged,
        'bikeair'       : bikeair,
        'bikeonly'      : bikeonly,
        'departdate'    : departdate,
        'returndate'    : returndate
    }

    currentRequest = $.ajax({
        url : "/tour-search-results/",
        data : getdata,
        method: "POST",
    }).done(function(data){
        submitting = false;

        //This checks to see what page the user had selected before they changed filter type
        //Before if you were on a page other than 1 and filtered for results that didn't populate past page 1
        //the results would be 0 of 0 even if tours existed.
        if(data.maxpage == 0  && paged != 1){
          pagedError = true;
          CMDB.tourJSON();
        } else {
          container.html(CMDB.buildData(data));
          CMDB.owlCarouselInit();
	        $(".tour-boxes").fadeIn();
          CMDB.svgToInlineSVG();
          pagedError = false;
        }

        lazyLoadInstance.update();
    });

}

/*
    Refine Buttons
    Pass dom element of the input or tracked form element to here to create a refine token for it.
*/
CMDB.createRefine = function(item) {

    var filterarea = $(".search-filters-top");

    if (item[0]) {
        if($("#refine-" + item[0].id).length > 0) {
            // testing
        } else {
            filterarea.append("<a class='refinefilter' id='refine-" + item[0].id + "' href='#' data-id='" + item[0].id + "'>" + item.data('name') + "<span>x</span></a>");
        }
    }

    $(".refinefilter").on("click", function(e){
        e.preventDefault();
        $("input#" + $(this).data('id')).prop('checked', false).removeClass("is-checked");
        //If refine button is a continent then uncheck all children countries
        if($("input#" + $(this).data("id")).hasClass('destination-group-input')){
          $("input#" + $(this).data("id")).parents('.destination-filter-box').find('.collapse input').prop('checked', false).removeClass("is-checked");
        }
        $(this).remove();
        CMDB.tourJSON();
    });

}

CMDB.removeRefine = function(item){
    var removeid = $("#refine-" + item[0].id);
    removeid.remove();
}


CMDB.getUrlVars = function(){
    var vars = [], hash;
    var activityLevel = [];
    var tourtype = [];
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        if(hash[0] == 'activitylevel%5B%5D') {
            activityLevel.push(hash[1]);
        } else if(hash[0] == 'tourtype%5B%5D' || hash[0] == 'tourtype') {
            tourtype.push(hash[1]);
        } else {
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
    }

    vars['activitylevel%5B%5D'] = activityLevel;
    vars['tourtype'] = tourtype;

    return vars;
}

// WHAT IS THIS! This is 9 hours of smashing your head against a wall till something comes out. jQuery event tracking
// was not working for reasons. Who knows. So we check to see if its a mouse click or not by a real mouse! if the position
// is 0 and 0 it wasnt a person it was a machine. Thats what this does. We could have done lots of checks on a multitude
// of other things but Safari, you know that awful horrible worse than IE now browser by that now horrible Apple company
// Yeah it decided to not get with the times and support anything. This is the work around.
CMDB.checkScreenPosition = function(obj) {
    if ( (obj.pageX && obj.pageX != 0) || (obj.pageY && obj.pageY != 0) ) {
        return true;
    } else {
        return false;
    }
}


// region level expand and fire search
CMDB.regionLevelExpand = function(hold) {
    $('.destination-group-input').on('click', function(e, hold){
        if ($(this).is(":checked")) {
            $(this).addClass('is-checked');
            $(this).parents('.destination-filter-box').find('.collapse input').prop('checked', true).addClass('is-checked');
        }else{
            $(this).removeClass('is-checked');
            $(this).parents('.destination-filter-box').find('.collapse input').prop('checked', false).removeClass('is-checked');
        }
        if (CMDB.checkScreenPosition(e)) {
            CMDB.tourJSON();
        }
    });
}

// country level expand and fire search
CMDB.countryLevelExpand = function(hold) {
    $('.filter-box-content-inner .custom-control-input').on('click', function(e, hold){
        if ($(this).is(":checked")) {
            $(this).addClass('is-checked');
            $(this).find("input").prop('checked', true);

            //If all countries within a continent are selected then automatically select continent and remove countries from refine bar
            if (!$(this).hasClass('destination-group-input')){
              var countriesArr = $(this).closest(".filter-box-content-inner").find('.custom-control-input');
              var allChecked = false;
              for (var i = 0; i < countriesArr.length; i++){
                if (countriesArr[i].classList.contains("is-checked")){
                  allChecked = true;
                } else{
                  allChecked = false;
                  i = countriesArr.length;
                }
              }
              if (allChecked && !$(this).closest(".destination-filter-box").find(".destination-group-input").is(":checked")){
                $(this).closest(".destination-filter-box").find(".destination-group-input").trigger("click");
              }
            }

            //If country already has refine and it's parent continent selected remove country from refine bar
            if ($(this).hasClass('destination-group-input')){
              var refines = $('.search-filters-top').children();
              for (var i = 0; i < refines.length; i++){
                if($(this).closest('.destination-filter-box').find('#' + refines[i].getAttribute('data-id')).length){
                  $('#' + refines[i].id).remove();
                }
              }
            }

          //If continent is checked already do not add refine for country
          if (!$(this).parents('.destination-filter-box').find('.destination-group-input').is(":checked") || $(this).hasClass('destination-group-input')){
            CMDB.createRefine($(this));
          }

        } else {
            $(this).removeClass('is-checked');
            $(this).find("input").prop('checked', false);
            //For the continent input
            $(this).closest(".destination-filter-box").find(".destination-group-input").removeClass('is-checked');
            $(this).closest(".destination-filter-box").find(".destination-group-input").prop('checked', false);
            CMDB.removeRefine($(this));
            CMDB.removeRefine($(this).closest(".destination-filter-box").find(".destination-group-input"));
            //Add back refines for countries still selected
            if (!$(this).hasClass('destination-group-input')) {
              countriesArr = $(this).closest(".filter-box-content-inner").find('.custom-control-input');
              for (var i = 0; i < countriesArr.length; i++) {
                if (countriesArr[i].classList.contains("is-checked")) {
                  CMDB.createRefine($("#" + countriesArr[i].id));
              }
            }
          }
        }
        if (CMDB.checkScreenPosition(e)) {
            CMDB.tourJSON();
        }
    });
}

//  tour type level select and fire search
CMDB.typeLevelSearch = function(hold) {
    $(".filter-box-content-inner .tour-type-list input ").on('click', function(e, hold){
        var input = $(this);
        if ( input.is(":checked") ) {
            input.addClass('is-checked');
            input.prop('checked', true);
            CMDB.createRefine(input);
        } else {
            input.removeClass('is-checked');
            input.prop('checked', false);
            CMDB.removeRefine(input);
        }
        if (CMDB.checkScreenPosition(e)) {
            CMDB.tourJSON();
        }
    });
}

CMDB.activityLevelSearch = function(hold) {
    $(".activity-level-filter-box .activity-level-list input").on('click', function(e, hold){
        var input = $(this);
        if ( input.is(":checked") ) {
            input.addClass('is-checked');
            input.prop('checked', true);
            CMDB.createRefine(input);
        } else {
            input.removeClass('is-checked');
            input.prop('checked', false);
            CMDB.removeRefine(input);
        }
        if (CMDB.checkScreenPosition(e)) {
            CMDB.tourJSON();
        }
    });
}



/* ========================================================================
	 * Search Filter Stuff
	 * ======================================================================== */

	CMDB.searchFilters = function(){

		// Activity Level Tooltips
		$('.activity-level-more-info').tooltipster({
			animation: 'fade',
			theme: 'tooltipster-shadow',
			// trigger: 'click'
		});

		$('.destinations-filters-box .collapse').on('hidden.bs.collapse', function () {
			
		});
		$('.destinations-filters-box .collapse').on('shown.bs.collapse', function () {
			
		});

        // DESTINATION FILTER
        CMDB.regionLevelExpand(true);
        CMDB.countryLevelExpand(true);
        CMDB.typeLevelSearch(true);
        CMDB.activityLevelSearch(true);

        // on load but lets also check for a tour.



        //default if landing on page
        if ($(".search-filters").length > 0) {
            var formvars = CMDB.getUrlVars();
            var dest = formvars['destination'];
            var tourtype = formvars['tourtype'];
            var activity = formvars['activitylevel%5B%5D'];
            var departure = formvars['departure'];
            var returndate = formvars['return'];

            // console.log(activity);

            // check everything that is needed then fire
            if (dest) {
                dest = dest.replace(/\s+/g, '-').toLowerCase();
                dest = dest.replace('+', '-');

                // check of united-states
                if (dest == 'united-states') {
                    dest = 'usa';
                }
                $('*[data-link=' + dest + ']').trigger('click', ["destinations"] ) ;
            }

            if (tourtype) {
                $.each(tourtype, function(index, element){
                    tourtype = element.replace(/\s+/g, '-').toLowerCase();
                    $('*[data-link=' + tourtype + ']').trigger('click', ["types"]);
                });
            }

            if (activity) {
                $.each(activity, function(index, element){
                    activity = element.replace(/\s+/g, '-').toLowerCase();
                    $('*[data-link=' + activity + ']').trigger('click', ["activities"]);
                });
            }

            if (departure && returndate) {


            }
            CMDB.tourJSON();
        }

        $(".booking-widget-return-date, .booking-widget-departuredate").on('change', function(e){
            CMDB.tourJSON();
        });

        $(".extra-filters .extra-filter-box").on('click', function(e){
            CMDB.tourJSON();
        });

        $('.sort-by-wrapper .wrapper-dropdown ul li a').on('click', function(evt){
            evt.preventDefault();
            CMDB.tourJSON();
        });


        if ($('.price-range-slider-box').length) {

            var priceRangeSlider = document.getElementById('price-range-slider-track');
            var priceRangeLow = document.getElementById('price-range-low');
            var priceRangeHight = document.getElementById('price-range-high');
            var inputs = [priceRangeLow, priceRangeHight];

            noUiSlider.create(priceRangeSlider, {
                start: [1000, 6000],
                // padding: [1000, 6000],
                step: 100,
                connect: true,
                range: {
                    'min': 0,
                    'max': 10000
                },
                format: {
                    to: function (value) {
                        return Math.round(value);
                    },
                    from: function (value) {
                        return Math.round(value);
                    }
                }
            });
            priceRangeSlider.noUiSlider.on('update', function (values, handle) {
                inputs[handle].value = "$" + values[handle];
                CMDB.tourJSON();
            });

            priceRangeSlider.noUiSlider.on('change', function (values, handle) {
                inputs[handle].value = "$" + values[handle];
                CMDB.tourJSON();
            });

            // Listen to keydown events on the input field.
            inputs.forEach(function (input, handle) {
                input.addEventListener('change', function () {
                    priceRangeSlider.noUiSlider.setHandle(handle, CMDB.parseCurrency(this.value));
                    CMDB.createRefine(input);
                });
                input.addEventListener('keydown', function (e) {
                    var values = priceRangeSlider.noUiSlider.get();
                    var value = Number(values[handle]);
                    // [[handle0_down, handle0_up], [handle1_down, handle1_up]]
                    var steps = priceRangeSlider.noUiSlider.steps();
                    // [down, up]
                    var step = steps[handle];
                    var position;
                    // 13 is enter,
                    // 38 is key up,
                    // 40 is key down.

                    CMDB.createRefine(input);

                    switch (e.which) {
                        case 13:
                            priceRangeSlider.noUiSlider.setHandle(handle, CMDB.parseCurrency(this.value));
                            break;
                        case 38:
                            // Get step to go increase slider value (up)
                            position = step[1];
                            // false = no step is set
                            if (position === false) {
                                position = 1;
                            }
                            // null = edge of slider
                            if (position !== null) {
                                priceRangeSlider.noUiSlider.setHandle(handle, value + position);
                            }
                            break;
                        case 40:
                            position = step[0];
                            if (position === false) {
                                position = 1;
                            }
                            if (position !== null) {
                                priceRangeSlider.noUiSlider.setHandle(handle, value - position);
                            }
                            break;
                    }
                });
            });
        }
    }

/* ========================================================================
   * Itinerary Table Month Filter
   * ======================================================================== */

  CMDB.itineraryMonthFilter = function(){

    $('.itinerary-select-wrapper .wrapper-dropdown ul li a').on('click', function(evt){
        evt.preventDefault();

        // Get the parent div so we only change it on the right dropdown
        var parent_div = $(this).closest('.itinerary-content-div').attr('id');

        // Get the accordion number
        var accordion_num = parent_div.split('-');
        accordion_num = accordion_num[1];

        // Get the date
        var selected_date = $(this).data('value');

        // if no more departures - value is 0
        if(selected_date !== 0){

          // Get the selected year
          var year = selected_date.substring(0, 4);

          // Split departure and arrival dates
          var date_pieces = selected_date.split('|');

          // Capture active year
          var active_pieces = date_pieces[0].split('-');
          var active_year = active_pieces[0];

          // Format into JS dates
          var depart_date = new Date(date_pieces[0].replace(/-/g, '/') + ' GMT');
          var return_date = new Date(date_pieces[1].replace(/-/g, '/') + ' GMT');

          // Fix user timezone issues back to UTC
          depart_date.setMinutes(depart_date.getMinutes() + depart_date.getTimezoneOffset());
          return_date.setMinutes(return_date.getMinutes() + return_date.getTimezoneOffset());

          // If the user is on the AIR Itin tab
          // Call the extension price filter to make sure we display the right price
          if (parent_div == 'content-1-itinerary') {
            CMDB.extensionPriceFilter(depart_date);
          }

          // Format the dates we want to display them
          var depart_date_formatted = formatDate(depart_date, '');
          var return_date_formatted = formatDate(return_date, 'year');

          // Merge it all together
          var new_date_formatted = depart_date_formatted + ' to ' + return_date_formatted;

          // Insert the new date into the date header
          $('#' + parent_div + ' .itin-date-header').text(new_date_formatted);

          // Hide all accordions within the parent tab and show the selected year only
          $('#' + parent_div + ' .itinerary-accordion').hide();
          $('#' + parent_div + ' #accordion-' + year + '-' + accordion_num).show();

          // Switch the extension to the right year
          $('.extension-wrapper').find('.extension-multi-year').removeClass('active-extension');
          $('.extension-wrapper').find('.extension-' + active_year).addClass('active-extension');

          // Now replace each itinerary day date
          $('#' + parent_div + ' #accordion-' + year + '-' + accordion_num + ' .date-title .date').each(function (index, value) {

            // Get the departure date
            var today = depart_date;

            // If this is not the first day add a day each time
            if (index > 0) {
              today.setDate(today.getDate() + 1);
            }

            // Format the date
            var today_formatted = formatDate(today, 'year');

            // Replace the old date
            $(this).text(today_formatted);

          });

          // Update PDF buttons - Show the right year
          $('.itin-pdf').each(function (index, value) {
            var pdf_url = $(this).attr('href');
            var pdf_pieces = pdf_url.split('year=');
            var pdf_vars = pdf_pieces[1].split('&package');
            var pdf_new_url = pdf_pieces[0] + 'year=' + year + '&package' + pdf_vars[1];
            $(this).attr('href', pdf_new_url);
          });
          $('.extension-pdf').each(function (index, value) {
            var pdf_url = $(this).attr('href');
            var pdf_pieces = pdf_url.split('year=');
            var pdf_vars = pdf_pieces[1].split('&package');
            var pdf_new_url = pdf_pieces[0] + 'year=' + year + '&package' + pdf_vars[1];
            $(this).attr('href', pdf_new_url);
          });
        }
      if(selected_date === 0) {
        // no departures to show - show default
        $('.itinerary-select-wrapper > .wrapper-dropdown > span').text('Next Departure');
      }

    });

  }


/* ========================================================================
   * Extension Price Filter - Based on the Itinerary Table Month Filter above
   * ======================================================================== */

  CMDB.extensionPriceFilter = function(depart_date){
    // Format the departure date for the extension filter
    var month = depart_date.getUTCMonth() + 1; //months from 1-12
    var day = depart_date.getUTCDate();
    var year = depart_date.getUTCFullYear();
    var extension_date = year+'-'+month+'-'+day;

    // Hide everything and then only show the selected date price
    // Pre data
    $('.pre_ext_pricing').hide();
    $('.pre_ext_pricing.date-'+extension_date).show();
    // Keep other years active
    $('.extensions-box').each(function(index) {
      if(!$(this).hasClass('extension-'+year)) {
        $(this).find('.pre_ext_pricing').first().show();
      }
    });

    // Post data
    $('.post_ext_pricing').hide();
    $('.post_ext_pricing.date-'+extension_date).show();
    // Keep other years active
    $('.extensions-box').each(function(index) {
      if(!$(this).hasClass('extension-'+year)) {
        $(this).find('.post_ext_pricing').first().show();
      }
    });
  }



/* ========================================================================
   * Pricing Table Month Filter
   * ======================================================================== */

  CMDB.priceMonthFilter = function () {
    $('.pricing-table .price-month-select-wrapper .wrapper-dropdown ul li a').on('click', function (evt) {
      evt.preventDefault();

      // Make sure this only effects this year price tab
      var year_tab = $(this).parents('.content').attr('id');

      // Get the month
      var selected_month = $(this).data('value');

      // Hide everything and then only show the selected month rows
    	$('.itinerary-dates-prices #'+year_tab+' .pricing-table .data-box').hide();
    	$('.itinerary-dates-prices #'+year_tab+' .pricing-table .data-box.month-'+selected_month+'').show();
    });
  }

/* ========================================================================
   * Pricing Table Airport Filter
   * ======================================================================== */

  CMDB.priceAirportFilter = function(){
    $('.pricing-table .airport-select-wrapper .wrapper-dropdown ul li a').on('click', function(evt) {
      evt.preventDefault();

	    var hideFull = 'false';
	    if($('.trigger-empty-rows-fetch').is(':checked')) {
	    	hideFull = 'true';
	    	$('.trigger-empty-rows-fetch').attr('checked', 'checked');
	    }else{
	    	hideFull = 'false';
	    }

      // Make sure this only effects this year price tab
      var year_tab = $(this).parents('.content').attr('id');

      if (!$(this).hasClass('other-city')) {
        // Set the dropdown to loading while we complete the ajax call
        $('#' + year_tab + ' .pricing-table .airport-select-wrapper .wrapper-dropdown span').text('Loading...');

        // Get the Airport
        var selected_airport = $(this).data('value');
        var selected_airport_name = $(this).text();

        // Get tour ID
        var tour_id = $(this).closest('.airport-select-wrapper').attr('data-tour-id');

        // Get the current year
        var tour_year = $(this).closest('.airport-select-wrapper').attr('data-tour-year');

        // Get the tour type (bike or walk)
        var tour_type = $(this).closest('.airport-select-wrapper').attr('data-tour-type');

        // Run Ajax Function - Used build in WP Ajax
        // See AjaxDisplayCallback() in \lib\softrip-data-calls.php
        setTimeout(function () {
          var data = {
            action: 'tour_dates_prices',
            tour_id: tour_id,
            hideFull: hideFull,
            airport: selected_airport,
            airport_name: selected_airport_name,
            tour_year: tour_year,
            tour_type: tour_type
          };

          $.post(ajaxurl, data, function (response, code) {
            if (response == 'fail') {

            } else {
              $("#" + year_tab + " .pricing-table").html(response).hide().fadeIn(500);
              // Re-enable all the filters and boxes on the new tap
              CMDB.customDropSetup();
              CMDB.customDropHandler();
              CMDB.priceMonthFilter();
              CMDB.priceAirportFilter();
              CMDB.itineraryDatesAndPricesTable();
            }
          });
        }, 1000);
      }//end if
      else{
        $('#'+year_tab+' .pricing-table .bottom .location').text('Other City (Price May Vary)');
        $('#'+year_tab+' .pricing-table .airport-select-wrapper .wrapper-dropdown span').text('Change Location >');
      }
    });
  }

/* ========================================================================
   * Pricing Table Empty Rows Fetch
   * ======================================================================== */


  CMDB.hideFullToursFilter = function(){

		$(document).on('change', '.trigger-empty-rows-fetch', function(evt) {

	    var hideFull = 'false';
	    if(this.checked) {
	    	hideFull = 'true';
	    }else{
	    	hideFull = 'false';
	    }

      // // Make sure this only effects this year price tab
      var year_tab = $(this).parents('.content').attr('id');

      // if (!$(this).hasClass('other-city')) {
        // Set the dropdown to loading while we complete the ajax call
        $('#' + year_tab + ' .pricing-table .airport-select-wrapper .wrapper-dropdown span').text('Loading...');

        // Get the Airport
        var selected_airport = $(this).parents('.pricing-table').find('.location').attr('data-airport');
        if (!$(selected_airport).length) {
        	selected_airport = 'New York, NY';
        }
        var selected_airport_name = $(this).parents('.pricing-table').find('.location').attr('data-airport-code');
        if (!$(selected_airport_name).length) {
        	selected_airport_name = 'JFK';
        }

        // Get tour ID
        var tour_id = $(this).parents('.pricing-table').find('.airport-select-wrapper').attr('data-tour-id');
        if (!$(tour_id).length) {
        	tour_id = $('#itinerary-dates-prices').attr('data-tour_id');
        }

        // Get the current year
        var tour_year = $(this).parents('.pricing-table').find('.airport-select-wrapper').attr('data-tour-year');
        if (!$(tour_year).length) {
        	tour_year = $(this).attr('data-year');
        }

        // Get the tour type (bike or walk)
        var tour_type = $(this).parents('.pricing-table').find('.airport-select-wrapper').attr('data-tour-type');
        if (!$(tour_type).length) {
        	tour_type = $('#itinerary-dates-prices').attr('data-tour_type');
        }

        // Run Ajax Function - Used build in WP Ajax
        // See AjaxDisplayCallback() in \lib\softrip-data-calls.php
        setTimeout(function () {
          var data = {
            action: 'tour_dates_prices',
            tour_id: tour_id,
            hideFull: hideFull,
            airport: selected_airport,
            airport_name: selected_airport_name,
            tour_year: tour_year,
            tour_type: tour_type
          };

          $.post(ajaxurl, data, function (response, code) {
            if (response == 'fail') {

            } else {
            	// console.log(response);
              $("#" + year_tab + " .pricing-table").html(response).hide().fadeIn(500);
              // Re-enable all the filters and boxes on the new tap
              CMDB.customDropSetup();
              CMDB.customDropHandler();
              CMDB.priceMonthFilter();
              CMDB.priceAirportFilter();
              CMDB.itineraryDatesAndPricesTable();
            }
          });
        }, 1000);
      // }//end if
      // else{
        // $('#'+year_tab+' .pricing-table .bottom .location').text('Other City (Price May Vary)');
        // $('#'+year_tab+' .pricing-table .airport-select-wrapper .wrapper-dropdown span').text('Change Location >');
        // $('#'+year_tab+' .pricing-table .airport-select-wrapper .wrapper-dropdown input').val(selected_airport);
      // }
    });
  }


  // build HTML
  CMDB.buildData = function(n) {
        // empty it first
        //container.empty();
        var output = "";

        // change count and page.
        var totalcount = jQuery(".results-total");
        totalcount.text(n.total);

        var currentresults = jQuery(".results-amount");
        currentresults.text(n.currentcount);
        CMDB.buildPagination(n);
        jQuery.each(n.tours, function(i, val) {

            output += '<div class="tour-boxes-wrap">';
            output += '<div class="tour-box tour-results-box">';

            output += '<div class="tour-results-box-img-carousel-container">';

            var current_favorites = getStorageItem( 'favorites' );
            if( !current_favorites ) {
                current_favorites = [];
            }
            var active_favorite = '';
            if (current_favorites.indexOf(val.ID) !== -1) {
                active_favorite = 'active';
            }

            output += '<div class="tour-box-favorite tour-box-favorite-results" data-post-id="' + val.ID + '">';
            output += '<i class="fi flaticon-heart ' + active_favorite + '"></i>';
            output += '</div>';

            output += '<div class="tour-results-box-img-carousel owl-carousel">';

            jQuery.each(val.images, function(i, newval){
                output += '<a href="'+val.link+'"><div class="item"><div class="img-wrap lazy" data-bg="url('+ newval +')" style="background-repeat: no-repeat; background-position: center center;"></div></div></a>';
                //output += '<div class="item"><div class="img-wrap" style="background: url(https://placehold.it/370x400) no-repeat center center;"></div></div>';
            });

            output += '</div>';
            output += '</div>';

            output += '<div class="tour-box-content">';
            output += '<div class="tour-box-content-top">';

            var current_compare = getStorageItem( 'compare' );
            if( !current_compare ) {
                current_compare = [];
            }
            var active_compare = '';
            if (current_compare.indexOf(val.ID) !== -1) {
                active_compare = 'checked';
            }

            output += '<div class="compare-tour custom-checkbox" data-post-id="' + val.ID + '"><input type="checkbox" class="custom-control-input extra" id="compare-tour-' + val.ID + '" name="compare" value="' + val.ID + '" ' + active_compare + '><label class="custom-control-label" for="compare-tour-' + val.ID + '"><span class="label">Compare</span></label></div>';

            output += '<p class="title-main"><a href="' + val.link + '">' + val.title + '</a></p>';

            output += '<p class="tour-type-title inline-tour-type-title ' + val.tourtype + ' ' + val.tourtypecheck +'">' + val.tourtypelabel +'</p>';

            if(val.prename) {
                output += '<p class="title-extension">Pre-Trip Extension: <strong>' + val.prename + '</strong></p>';
            }
            if(val.postname) {
                output += '<p class="title-extension">Post-Trip Extension: <strong>' + val.postname + '</strong></p>';
            }
            // output += '<p class="title-duration">Duration: <span>'+ val.duration +' Days</span></p>';

            // IF REVIEWS

            if (val.reviewavg > 0) {
                output += '<div class="tour-box-ratings">';
                output += '<ul class="stars">';

                jQuery.each(val.reviewavgstars, function(val){
                    output += '<li class="star"><a class="ratingStar active" data-value="1" href="#" title="Start 1"><img src="/content/themes/base/img/star.svg" alt="Star" class="svg"></a></li>';
                });

                output += '</ul>';
                output += '<div class="reviewsLink"> <span>' + val.reviewavg + ' out of 5 stars.</span> <a href="'+val.link+'reviews/">Read Reviews</a></div>';
                output += '</div>';
            }

            output += '</div>';
            // End .tour-box-content-top




            // ACTIVITY ICONS
            output += '<div class="activity-icons">';

            //TODO make sure we have different levels setup here.
            if (val.level) {

                var iconName = "";

                switch (val.level) {
                    case "easymoderate":
                        iconName = "Easy / Moderate";
                        break;
                    case "easy":
                        iconName = "Easy";
                        break;
                    case "activity-moderate-challenging":
                        iconName = "Moderate / Challenging";
                        break;
                    case "moderate":
                        iconName = "Moderate";
                        break;
                    default:
                        iconName = "";
                        break;
                }

                output += '<figure>';
                output += '<img src="/content/themes/base/img/activity-' + val.level + '.svg" alt="'+ val.level + ' Activity">';
                output += '<figcaption>'+ iconName + ' Activity</figcaption>';
                output += '</figure>';

            }



            if (val.typecat) {
                $.each(val.typecat, function(inx, n){
									
									var iconName = "";
									switch (n) {
										case "activity-easy-moderate":
											iconName = "Easy Moderate";
										    break;
										case "activity-easy":
											iconName = "Easy";
										    break;
										case "activity-moderate-challenging":
											iconName = "Moderate / Challenging";
										    break;
										case "activity-moderate":
											iconName = "Moderate";
										    break;
										case "airplane":
											iconName = "Air Package Available";
										    break;
										case "biking":
											iconName = "Bike Included";
											n = "bike";
										    break;
                                        case "self-guided-biking":
                                            iconName = "Bike Included";
                                            n = "bike";
                                            break;
										case "boat":
											iconName = "Boat Tour";
										    break;
                                        case "barge-and-sail":
										    iconName = "Boat Tour";
										    n = "boat";
										    break;
										case "bus":
											iconName = "Vehicle Support";
										    break;
										case "e-bike":
											iconName = "E-Bikes Available";
										    break;
										case "people":
											iconName = "Guided Group Tour";
										    break;
										case "self-guided":
											iconName = "Self Guided Tour";
										    break;
										case "walking":
											iconName = "Walking Tour";
										    break;

										default:
											iconName = "";
											break;
									}

		                output += '<figure>';
			                output += '<img src="/content/themes/base/img/'+ n +'.svg" alt="' + iconName + '">';
			                output += '<figcaption>' + iconName + '</figcaption>';
		                output += '</figure>';
                });
            }
            output += '</div>';

            output += '<div class="tour-box-content-bottom">';
            output += '<a href="' + val.link + '" class="btn btn-blue">View Tour</a>';
            output += '<a href="' + val.link + '#itinerary-dates-prices" class="btn inline">Book Online ></a>';
            output += '</div>';

            output += '</div>';

            output += '<div class="tour-box-sidebar">';
            output += '<div class="tour-box-sidebar-inner">';

            output += '<div class="pricing-block">';
            output += '<p class="pricing-starting-from">Starting From</p>';

            var tourtypedisplay = '';
            if(val.tourtypecheck === 'biking' || val.tourtypecheck === 'bike-and-boat' ||  val.tourtypecheck === 'sg-biking'){
              tourtypedisplay = 'Bike';
            }
            if(val.tourtypecheck === 'walking'){
              tourtypedisplay = 'Walking';
            }

            var airpackagedisplay = ' +Air Package ';
	         var vacKey = tourtypedisplay + ' Vacation';
	         if(val.tourtype === 'self-guided'){
              vacKey = '';
		        airpackagedisplay = ' Self-Guided Air+ ';
	         }

            if (val.price != 0) {
                output += '<p class="pricing-disclaimer">'+ val.duration +'-day '+ vacKey +'</p>';
                output += '<p class="pricing-disclaimer">' + airpackagedisplay + '</p>';
                output += '<p class="pricing-price blue"><span class="dollarSign">$</span><span>'+ val.price + '</span></p>';
            }
            if (val.priceland) {
                output += '<p class="pricing-disclaimer">'+ val.landduration +'-day '+ tourtypedisplay +'</p>';
                output += '<p class="pricing-disclaimer">Vacation Only</p>';
                output += '<p class="pricing-price"><span class="dollarSign">$</span><span>' + val.priceland + '</span></p>';
            }
            if (val.price || val.priceland ) {
                output += '<p><i>Per person, double occupancy</i></p>';

            } else {
                output += '<p class="pricing-disclaimer">Call for information.</p>';
                output += '<p><strong><a href="tel:18002453868">1.800.245.3868</a></strong></p>';
            }

            output += '</div>';

            // output += '<div class="tour-box-content-bottom">';
            // output += '<a href="' + val.link + '" class="btn btn-blue">View Tour</a>';
            // output += '<a href="' + val.link + '#itinerary-dates-prices" class="btn inline">Book Online ></a>';
            // output += '</div>';

            output += '</div>';
            output += '</div>';

            output += '</div>';

            output += '</div>';
            output += '</div>';

        });
        return output;
    }


    // build pagination
    CMDB.buildPagination = function(n) {

        var placement = $(".pagination");
        placement.empty();

        if (n.maxpage > 1) {
            for (var i = 1; i <= n.maxpage; i++ ) {
                if (n.paged == i) {
                    placement.append("<span class='active'>" + i + "</span>");
                } else {
                    placement.append("<span>" + i + "</span>");
                }
            }
        }

        placement.find("span").on('click', function(evt){
            evt.preventDefault();
            placement.data( "paged" , $(this).index() + 1 );
            CMDB.tourJSON();
        });

    }

    CMDB.parseCurrency = function(n) {
        return Number(n.replace(/[^0-9.-]+/g,""));
    }


  /* ========================================================================
   * Video popup helper
   * ======================================================================== */


	CMDB.videoPopupCloseHelper = function(){

		if ($('.videoModal').length) {
			$('.videoModal').on('hidden.bs.modal', function (e) {
				// check for youtube or vimeo
				$(this).find('iframe').attr('src', $('.videoModal').find('iframe').attr('src'));

				// check for html5 video
				var video = $(".videoClose").get(0);
				if (video) {
					if (video.paused) {
					}else{
						video.pause();
					}
				}
			})
		}
	};


  /* ========================================================================
   * Video popup helper
   * ======================================================================== */

	CMDB.bikeSpecsAccordions = function(){
		if ($('.bike-box-container').length) {
			$('.bike-hidden-boxes').on('hide.bs.collapse', function (e) {
				// $(this).parents('.bike-box-container').removeClass('active');
				$("[data-target='#" + $(e.target).attr('id') + "']").parent().removeClass('active');
			})
			$('.bike-hidden-boxes').on('show.bs.collapse', function (e) {
				// $(this).parents('.bike-box-container').addClass('active');
				$("[data-target='#" + $(e.target).attr('id') + "']").parent().addClass('active');
			})
			$('.bike-hidden-boxes').on('hidden.bs.collapse', function (e) {
				$(this).parents('.bike-box-container').removeClass('active');
				// $("[data-target='#" + $(e.target).attr('id') + "']").parent().removeClass('active');
			})
			$('.bike-hidden-boxes').on('shown.bs.collapse', function (e) {
				$(this).parents('.bike-box-container').addClass('active');
				// $("[data-target='#" + $(e.target).attr('id') + "']").parent().addClass('active');
			})
		}
	}


  /* ========================================================================
   * Stop Video from playing when modal closes
   * ======================================================================== */

  CMDB.stopVideoModals = function(){

    if ($('#videoModalFullWidthCTA').length) {
      $('#videoModalFullWidthCTA').on('hidden.bs.modal', function (e) {

        // Get the iframe and see if this is a Vimeo or Youtube video
        var video = $('#videoModalFullWidthCTA').find('iframe');

        // For Vimeo we included the Vimeo JS file in the layout file
        if(video.hasClass('videoModalFullWidthCTA-vimeo')) {
          // Vimeo JS allows us to start a new player and pause it
          var player = new Vimeo.Player(video);
          player.pause();
        }
        else {
          // The YouTube embed has included the enablejsapi, this allows us to pauseVideo below
          video[0].contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        }

      });
    }
    
  };


  /* ========================================================================
   * General Functions
   * ======================================================================== */

	CMDB.generalFunctions = function(){
		$('.stories-share-box-link').on('click', function(e){
			e.preventDefault();
			if ($('#atic_more').is(":visible")) {
				$('#atic_more').trigger('click');
			}
		})

		// header search box focus on open
		$('#search').on('shown.bs.collapse', function () {
		  $('#header-search').focus();
		})

		// header search box close on click outside
		if ($('#search').length) {
			$(document).click(function(e) {
				if ($(e.target).parents('#search').length) {
					// return false;
				}else{
					$('#search').collapse('hide');
				}
			});
		}
	}

/* ========================================================================
 * Favorites
 * ======================================================================== */

    CMDB.loadFavorites = function(){
        // Get all favorites.
        var favorites = getStorageItem( 'favorites' );
        var no_favorites_text = $('#favorite-tours-list').data('no-favorites');

        if (favorites === undefined || favorites.length == 0) {
            $('#favorite-tours-list').append('<div class="no-favorites">' + no_favorites_text + '</div>');
        }

        favorites.forEach(function(favorite) {
            // Get favorite tours.
            $.ajax({
                url: '/wp/wp-admin/admin-ajax.php',
                data: {action:'favorite_tours',tour_id:favorite},
                type: 'POST',
                success: function (data) {
                    $('#favorite-tours-list').append(data);
                    // Tour Box carousels init
                    var tourBoxesCarousel = $('.tour-box-img-carousel');
                    tourBoxesCarousel.owlCarousel({
                        items:1,
                        loop:false,
                        dots: true,
                        nav: true,
                        lazyLoad: true,
                        navText: ['<span><img class="owlNavArrows" src="/content/themes/base/img/caret-left.svg" alt="Left Arrow" /></span>', '<span><img class="owlNavArrows" src="/content/themes/base/img/caret-right.svg" alt="Right Arrow" /></span>'],
                    });
                    CMDB.tourBoxCompare();
                }
            });
        });
    };

    /* ========================================================================
     * Compare Tool
     * ======================================================================== */

    CMDB.loadCompareTool = function(){
        // Get all tours to compare.
        var compare = getStorageItem( 'compare' );

        if( !compare ) {
            compare = [];
        }

        if (compare.length < 1 || compare == undefined) {
            // No tours to compare, clear compare tool.
            compare = [];
            $('#compare-tour').fadeOut().find('.tours').html('');
            closeCompare();

            return false;
        }

        var tours_total = compare.length;
        var tour_count = 0;

        if (tours_total > 1) {
            $('#compare-tour .compare-tours a').removeClass('disabled').text('Compare Tours');
        } else {
            $('#compare-tour .compare-tours a').addClass('disabled').text('Add a Tour to Compare');
        }

        var display_promises = [];
        $.each(compare, function(index, item) {
            // Show tours to compare.
            display_promises.push(
                $.ajax({
                    url: '/wp/wp-admin/admin-ajax.php',
                    data: {action:'compare_tool',tour_id:item},
                    type: 'POST',
                    success: function (data) {
                        if (tour_count) {
                            $('#compare-tour .tours').append(data);
                        } else {
                            $('#compare-tour').css('display', 'flex').fadeIn().find('.tours').html(data);
                        }
                        tour_count++;
                    }
                })
            );
        });

        $.when.apply($, display_promises).then(function() {
            if (tours_total < 4) {
                var remainder = 4 - tours_total;
                var current_item = 0;
                var i;
                for (i = 0; i < remainder; i++) {
                    current_item = tours_total + 1 + i;
                    $('#compare-tour .tours').append('<div class="tour empty-tour"><p>' + current_item + '</p></div>');
                }
            }
        });

        $('#compare-tour .tour-count .current').html(tours_total);
    };

    /* ========================================================================
     * Predictive Search while typing
     * Uses AJAX predictive_search_function() in \lib\custom.php
     * ======================================================================== */

    CMDB.predictiveSearch = function(){
        
        // Declare vars
        var keywords = '',
            search_characters = 0,
            element,
            delayTimer;

        // Trigger when user starts typing in the search box
        $('.predictive-search').keyup(function() {

            // Save the keywords to pass on via ajax
            keywords = $(this).val();

            // Save length of search string
            search_characters = keywords.length;
            element = $(this);

            // Trigger predictions only after 3 characters are typed.
            if (search_characters > 2) {

                // Wait a second before actually searching after they stop typing
                clearTimeout(delayTimer);
                delayTimer = setTimeout(function() {

                  // Get predictions based on current search term.
                  $.ajax({
                      url: '/wp/wp-admin/admin-ajax.php',
                      data: {action:'predictive_search',keywords:keywords},
                      type: 'POST',
                      success: function (data) {
                          $(element).parent().find('.predicted-results').html(data).fadeIn();
                          $('.predicted-results li').on('click', function() {
                              $(element).parent().find('.predictive-search').val($(this).text());
                              $('.predicted-results').fadeOut();
                          });
                      }
                  });
                }, 1000); // endif timer (Will do the ajax stuff after 1000 ms, or 1 s)
            } // endif(search_characters > 2)
        });
    };

  /* ========================================================================
* Welcome Banner stuff
* ======================================================================== */

  CMDB.welcomeBannerInit = function () {
    // set up the close button
    if ($('#welcome-banner-close-button').length) {
      $('#welcome-banner-close-button').on('click', function (e) {
        e.preventDefault();
        $('.site-wrap').removeClass('with-banner').removeClass('topstick');
        $('#header').removeAttr('style');
        $('#main-content').removeAttr('style');
	      if ($('body').hasClass('single-post')) {
		      $('#hero').removeAttr('style');
		    }
        document.cookie = "hideBanner=true;max-age=86400";
        CMDB.stickyMenu(true);
      })
    }
    if (bannerCookieExists()) {
      $('.site-wrap').removeClass('with-banner').removeClass('topstick');
      $('#welcomeBanner').css('display', 'none');
    }
    // set up the media query
    if ($('#welcomeBanner').length && !bannerCookieExists()) {
      var tablet = window.matchMedia('(min-width: 992px)');
      tablet.addListener(detectWelcomeBannerHeight);
      detectWelcomeBannerHeight(tablet);
      window.addEventListener('resize' ,detectWelcomeBannerHeight)
    }
  };

  /* ========================================================================
   * Review sorting stuff
   * ======================================================================== */

  CMDB.reviewSorting = function () {

    if ( $('.review-sort').length ) {

      $('.review-sort .wrapper-dropdown ul li a').on('click', function(evt){
        $('.review-sort').submit();
      });

      // Preselect the dropdown option based on url param
      var urlVars = CMDB.getUrlVars();
      if ( typeof urlVars['sort-by'] != 'undefined' ) {
        $('.review-sort .wrapper-dropdown > span').text($('.dropdown a[data-value='+urlVars['sort-by']+']').text());
      }

    }

  };

  var detectWelcomeBannerHeight = debounce(function (t) {
    var tablet = window.matchMedia('(min-width: 992px)');
    if (t.matches || tablet.matches) {
      $('#header').css('top', $('#welcomeBanner').outerHeight());
      var mt = ($('#welcomeBanner').outerHeight() + 18);

      if ( !$('body').hasClass('home') ) {
        mt += $('#welcomeBanner').outerHeight();
      }
      $('#main-content').css('margin-top', mt);
      if ($('body').hasClass('single-post')) {
      	if ($('#hero').length) {
      		$('#hero').css('margin-top', mt);
      		$('#main-content').css('margin-top', '0');
      	}else{
      		var breadcrumbPaddingTop = parseInt($('.breadcrumb').css('padding-top'));
      		var headerHeight = $('#header').outerHeight();
      		var bannerHeight = $('#welcomeBanner').outerHeight();
					$('#main-content').css('margin-top', (bannerHeight+headerHeight)-breadcrumbPaddingTop)+"px";
      	}
      }

    } else {
      $('#header').removeAttr('style');
      $('#main-content').removeAttr('style');
      if ($('body').hasClass('single-post')) {
	      $('#hero').removeAttr('style');
	    }
    }
  }, 200);

  function bannerCookieExists() {
    var cookieArray = document.cookie.split(';');
    var key, value;
    for (var i=0; i<cookieArray.length; i++) {
      key = cookieArray[i].split('=')[0];
      value = cookieArray[i].split('=')[1];
      if (key.trim() === "hideBanner"){
        return true;
      }
    }
    return false;
  }

  // Custom Gravity Forms hooks
	CMDB.gravityFormsCustom = function () {
		$(document).on('gform_page_loaded', function(event, form_id){
			// code to be trigger when next/previous page is loaded
			var form = $("#gform_wrapper_"+form_id);
			// var parent = form.closest(".basic-accordion-container");
			var parent = form.closest(".accordion-body");
			if(parent.length > 0 ){
				var offset = parent.offset().top;
				if(!isMobile()){
					offset = parent.offset().top - $(".menu-bar").height();
				}
				$("html, body").animate({scrollTop: offset});
			}
		});
	}

  /* ========================================================================
   * Make row CTAs equal height
   * ======================================================================== */

  CMDB.equalRowHeightCta = function(parent, container){
    $(parent).each(function() {
      var currentTallest = 0,
          currentRowStart = 0,
          rowDivs = new Array(),
          $el, topPosition = 0,
          currentDiv = 0;
      $(this).find(container).each(function() {
        $el = $(this);
        $($el).height('auto');
        topPosition = $el.position().top;

        if (currentRowStart != topPosition) {
          for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
            rowDivs[currentDiv].height(currentTallest);
          }
          rowDivs.length = 0; // empty the array
          currentRowStart = topPosition;
          currentTallest = $el.height();
          rowDivs.push($el);
        } else {
          rowDivs.push($el);
          currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
        }
        for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
          rowDivs[currentDiv].height(currentTallest);
        }
      });
    });
  }
  
  /* ========================================================================
   * FUNCTION CALLS
   * ======================================================================== */

  //Window load functions
  // $window.load(function() {

  // });

  //Document ready functions
  $document.ready(function() {
    CMDB.worldMapPopup();
    CMDB.regionMapPopup();
    CMDB.countryMapPopup();
    CMDB.megaMenuStuff();
    CMDB.stickyMenu();
    CMDB.tourBoxFavorite();
    CMDB.tourBoxCompare();
    CMDB.svgToInlineSVG();
    CMDB.setupDateRange();
    CMDB.customDropSetup();
    CMDB.customDropHandler();
    CMDB.tabcordion();
    CMDB.itineraryPagePopup();
    CMDB.itineraryPageNav();
    CMDB.itineraryMenu();
    CMDB.itineraryDatesAndPricesTable();
    CMDB.itineraryPagePerPostCollapseBox();
    CMDB.itineraryExtensionYearSwitch();
    CMDB.itineraryCollapseOutsideReigion();
    CMDB.bikingItineraryCollapseToggle();
    CMDB.setupChangeEvents();
    CMDB.setupBlogListing();
    CMDB.setupBlogSearch();
    CMDB.setupBlogClick();
    CMDB.searchFilters();
    CMDB.itineraryMonthFilter();
    CMDB.priceMonthFilter();
    CMDB.hideFullToursFilter();
    CMDB.priceAirportFilter();
    CMDB.owlCarouselInit();
    CMDB.videoPopupCloseHelper();
    CMDB.stopVideoModals();
    CMDB.customDestinationPicker();
    CMDB.bikeSpecsAccordions();
    CMDB.generalFunctions();
    CMDB.itineraryCollapseToggle();
    CMDB.basicAccordianToggle();
    CMDB.loadCompareTool();
    CMDB.predictiveSearch();
    CMDB.welcomeBannerInit();
    CMDB.reviewSorting();
    CMDB.gravityFormsCustom();

    if ($('#favorite-tours-list').length) {
        CMDB.loadFavorites();
    }

    // CMDB.bLazy();
  });

  //IE10 only fix
  if (navigator.userAgent.match('MSIE 10.0;')) {
    $('html').addClass('ie10');
  }

})(jQuery);

// May be temporary, it wasn't getting accurate info in setup its handler correctly with this above/below setup
$(window).on("load", function (e) {
    CMDB.itineraryMenu(true);
    
    var windowWidth = window.innerWidth || $(window).width();
    // Not on mobile
    if(windowWidth > 767) {
      CMDB.equalRowHeightCta('.bikes .row', '.bike-box-container');
    }
});


$(window).on("resize", function (e) {
    CMDB.itineraryMenu(true);

    var windowWidth = window.innerWidth || $(window).width();
    // Not on mobile
    if(windowWidth > 767) {
      CMDB.equalRowHeightCta('.bikes .row', '.bike-box-container');
    }
});

// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

// When opening a description box in the itinerary, adjust the carousel to match
function changeItinSlider( newSlide ) {
    var itinSlider = $( '.biking-itinerary-carousel' );
    itinSlider.trigger( 'to.owl.carousel', [ ( newSlide - 1 ), 0 ] );
}

$( function() {
    var owlDots = $( '.owl-dot span' );
    owlDots.each( function() {
       $( this ).append( '<em class="hidden">Slider navigation icon</em>' );
    });
});



// Polyfill for .includes for IE
// Added by mark.theriault
// May 23 2019
// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(valueToFind, fromIndex) {

      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      // 1. Let O be ? ToObject(this value).
      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(valueToFind, elementK) is true, return true.
        if (sameValueZero(o[k], valueToFind)) {
          return true;
        }
        // c. Increase k by 1. 
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

// Pollyfil for Object.assign
if (!Object.assign) {
  Object.defineProperty(Object, 'assign', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(target) {
      'use strict';
      if (target === undefined || target === null) {
        throw new TypeError('Cannot convert first argument to object');
      }

      var to = Object(target);
      for (var i = 1; i < arguments.length; i++) {
        var nextSource = arguments[i];
        if (nextSource === undefined || nextSource === null) {
          continue;
        }
        nextSource = Object(nextSource);

        var keysArray = Object.keys(Object(nextSource));
        for (var nextIndex = 0, len = keysArray.length; nextIndex < len; nextIndex++) {
          var nextKey = keysArray[nextIndex];
          var desc = Object.getOwnPropertyDescriptor(nextSource, nextKey);
          if (desc !== undefined && desc.enumerable) {
            to[nextKey] = nextSource[nextKey];
          }
        }
      }
      return to;
    }
  });
}

/************************************************************
 * Function below handles HTML 5 video support on tour pages
 ************************************************************/
(function () {
  'use strict';

  // Does the browser actually support the video element?
  var supportsVideo = !!document.createElement('video').canPlayType;

  if (supportsVideo) {
    // Obtain handles to main elements
    var videoContainer = document.getElementById('videoContainer');
    var video = document.getElementById('basevideoholder');
    var videoControls = document.getElementById('videoContainerControls');

    // Display the user defined video controls
    if(video !== null && videoControls !== null) {

      // Hide the default controls
      if(video.controls.length) {
        video.controls = false;
      }

      // Display the user defined video controls
      videoControls.setAttribute('data-state', 'visible');

      // Obtain handles to buttons and other elements
      var mute = document.getElementById('videoContainerMute');

      // Only add the events if addEventListener is supported (IE8 and less don't support it, but that will use Flash anyway)
      if (document.addEventListener) {

        // Add events for mute button
        mute.addEventListener('click', function(e) {
          video.muted = !video.muted;
          mute.setAttribute('data-state', video.muted ? 'unmute' : 'mute');
          if(video.muted == true) {
            $('#videoContainerMute').html('<img src="/content/themes/base/img/sound-on-white.svg" alt="Unmute">');
          } else {
            $('#videoContainerMute').html('<img src="/content/themes/base/img/sound-off-white.svg" alt="Mute">');
          }
        });

      }
    }
  }

})();

/************************************************************
 * Pass UTM cookies on to Softrip
 ************************************************************/
var createCookie = function(name, value, days) {
  var expires;
  if (days) {
    var date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toGMTString();
  }
  else {
    expires = "";
  }
  document.cookie = name + "=" + value + expires + ";domain=.vbt.com;path=/";
}
var getUrlVars = function() {
  var vars = {};
  var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
    vars[key] = value;
  });
  return vars;
}


jQuery(function(){
  vars = getUrlVars();
  if(vars['utm_source']){
    createCookie('utm_source', vars['utm_source'], 10);
    createCookie('utm_medium', vars['utm_medium'], 10);
    createCookie('utm_campaign', vars['utm_campaign'], 10);
  }
});